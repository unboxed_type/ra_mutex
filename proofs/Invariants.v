Require Import Init Bool NPeano List StepStarTheory Arith Program Fin .
Require Import JRWTactics.
Require Import System.
Import ListNotations.

Definition invariant_init (P: GState -> Prop) :=
  P Ginit_state.

Definition invariant_step (P: GState -> Prop) :=
  forall op s s',
    P s 
    -> step s op s' 
    -> P s'.

Definition invariant (P: GState -> Prop) :=
  invariant_init P /\ invariant_step P.

(***
  The generalization of the invariance rule: 
    Invariant(P) holds if, for some predicate Q 
      1) Invariant(Q) holds, 
      2) P holds initially, and 
      3) for every statement S of the program, executing
         S when P /\ Q holds leaves P holding.
      This rule allows us to assume the invariance
      of Q, and we make use of it in proving Invariant(P). 
***)

Definition invariant_step2 (P: GState -> Prop) (Q: GState -> Prop) :=
  forall op s s',
    P s
    -> Q s 
    -> step s op s'
    -> P s'.
Definition invariant2 (P: GState -> Prop) (Q: GState -> Prop) :=
  invariant Q
  -> invariant_init P /\ invariant_step2 P Q.

Definition invariant_step3 (P Q R : GState -> Prop) :=
  forall op s s',
    P s
    -> Q s
    -> R s
    -> step s op s'
    -> P s'.

Definition invariant3 P Q R :=
  invariant R
  -> invariant2 Q R
  -> invariant_init P /\ invariant_step3 P Q R.

Definition invariant_step4 (P Q R Z: GState -> Prop) :=
  forall op s s',
    P s
    -> Q s
    -> R s
    -> Z s
    -> step s op s'
    -> P s'.

Definition invariant4 P Q R Z :=
  invariant Z
  -> invariant3 Q R Z
  -> invariant_init P /\ invariant_step4 P Q R Z.

Theorem step_star_inv':
    forall P t s,
    invariant P 
    -> step_star Ginit_state t s 
    -> P s.
Proof.
  intros.
  generalize dependent s.
  induction t using rev_ind.
  intros s H1.
  inversion H1; subst. unfold invariant in H. inversion H. unfold invariant_init in H0.
  assumption.
  apply app_eq_nil in H0. 
  inversion H0. inversion H5.
  intros.
  apply step_star_app_one in H0. inversion H0.
  unfold invariant in H. inversion H.
  unfold invariant_step in H3.
  inversion H1.
  apply IHt in H4.
  eauto.
Qed.

Theorem step_star_inv2:
  forall P Q t s,
    invariant Q
    -> invariant2 P Q
    -> step_star Ginit_state t s 
    -> P s.
Proof.
  intros.
  generalize dependent s.
  induction t using rev_ind.
  intros.
  inversion H1; subst.
  unfold invariant2 in H0. apply H0 in H.
  inversion H.
  unfold invariant_init in H2.
  assumption.
  apply app_eq_nil in H2. 
  inversion H2. inversion H6.
  intros.
  assert (H'':=H).
  apply step_star_app_one in H1. inversion H1.
  inversion H2.
  unfold invariant2 in H0. apply H0 in H.
  inversion H. unfold invariant_step2 in H4.
  unfold invariant_step2 in H6.
  apply H6 with (op:=x) (s:=x0).
  apply IHt.
  assumption.
  eapply step_star_inv' in H''.
  eassumption.
  eassumption.
  assumption.
Qed.

Theorem step_star_inv3:
  forall P Q R t s,
    invariant R
    -> invariant2 Q R
    -> invariant3 P Q R
    -> step_star Ginit_state t s 
    -> P s.
Proof.
  intros P Q R t s Hr Hqr Hpqr Hss.
  generalize dependent s.
  induction t using rev_ind.
  intros.

  inversion Hss; subst.
  unfold invariant3 in Hpqr. apply Hpqr in Hr.
  inversion Hr. unfold invariant_init in H. assumption.
  assumption.
  apply app_eq_nil in H. 
  inversion H. inversion H3.

  intros.
  apply step_star_app_one in Hss. inversion Hss.
  inversion H.
  unfold invariant3 in Hpqr.
  assert (Hr' := Hr).
  apply Hpqr in Hr'.
  inversion Hr'; clear Hr'.
  unfold invariant_step3 in H3.
  eapply H3.
  eapply IHt.
  eassumption.
  eapply step_star_inv2.
  eassumption.
  assumption.
  eassumption.
  eapply step_star_inv'.
  assumption.
  eassumption.
  eassumption.
  assumption.
Qed.

Theorem step_star_inv4:
  forall P Q R Z t s,
    invariant Z
    -> invariant2 R Z
    -> invariant3 Q R Z
    -> invariant4 P Q R Z
    -> step_star Ginit_state t s 
    -> P s.
Proof.
  intros P Q R Z t s Hr Hqr Hpqr Hss.
  generalize dependent s.
  induction t using rev_ind.
  intros.

  inversion H; subst; clear H.
  unfold invariant4 in *. 
  apply Hss in Hr.
  break_and.
  assumption.
  assumption.  
 
  find_apply_lem_hyp app_eq_nil.
  break_and.
  congruence.
  
  intros.
  find_apply_lem_hyp step_star_app_one. break_exists.
  break_and.  
  unfold invariant4 in *.
  copy_apply Hss Hr.
  break_and.
  unfold invariant_step4 in *.
  eapply H2.
  eapply IHt.
  eassumption.
  eapply step_star_inv3.
  eassumption.
  eassumption.
  eassumption.
  eassumption.
  eapply step_star_inv2.
  eassumption.
  eassumption.
  eassumption.
  eapply step_star_inv'.
  eassumption.
  eassumption.
  eassumption.
  eassumption.
Qed.

