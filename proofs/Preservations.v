Require Import Init Bool NPeano List StepStarTheory Arith Program Fin .
Require Import JRWTactics Clients System Invariants Automation.

Import ListNotations.

Lemma update_get_self_eq: 
  forall s i p, update s i p i = p.
Proof. 
  unfold update. intros. rewrite ite_id_dec_true. reflexivity. 
Qed.

Hint Rewrite update_get_self_eq : defs.

(** Lemmas about send_next_request,send_next_reply are the 
    most important. They are at the heart of autorewrite *)
Lemma preserve_clock_send_next_request:
  forall i j j_clk l st,
   clock (send_next_request l j j_clk st i) = clock (st i).
Proof.
  induction l. simpl. reflexivity. intros. simpl. rewrite IHl. molotok. 
Qed.

Lemma preserve_clock_send_next_reply:
  forall l i i_clk st j,
   clock (send_next_reply l i i_clk st j) = clock (st j).
Proof. induction l. simpl.  reflexivity. intros.  simpl.
       molotok. rewrite IHl. molotok. Qed.

Lemma preserve_clock_send_request:
  forall j i a i_clk st,
    clock (st j) = clock (send_request a i i_clk st j).
Proof. molotok. Qed.

Lemma preserve_clock_update_fsm_state:
  forall i j instr st,
    clock (update_fsm_state instr j st i) = clock (st i).
Proof. molotok. Qed.

Lemma preserve_clock_send_reply:
  forall i i_clk j p j_clk st,
    clock (send_reply i i_clk j j_clk st p) = clock (st p).
Proof. molotok. Qed.

Lemma preserve_clock_update_receive_from:
  forall i j st l,
   clock (update_received_from l i st j) = clock (st j).
Proof. molotok. Qed.

Lemma preserve_clock_remove_msg:
  forall i j st,
   clock (remove_msg i st j) = clock (st j).
Proof. molotok. Qed.

Lemma preserve_clock_update_await_from:
  forall af i j st,
    clock (update_await_from af i st j) = clock (st j).
Proof. molotok. Qed.

Lemma preserve_clock_update_deferred_clock:
  forall i j c st,
    clock (update_deferred_clock c i st j) = clock (st j).
Proof. molotok. Qed.

Lemma preserve_clock_update_reply_sent:
  forall i j r st, clock (update_reply_sent r i st j) = clock (st j).
Proof. molotok. Qed.

Lemma preserve_clock_acquire_lock:
  forall i j st, clock (acquire_lock i st j) = clock (st j).
Proof. molotok. Qed.

Hint Rewrite 
     preserve_clock_send_next_reply
     preserve_clock_send_next_request
     preserve_clock_remove_msg
     preserve_clock_update_await_from
     preserve_clock_update_receive_from
     preserve_clock_send_reply
     preserve_clock_update_fsm_state
     preserve_clock_update_reply_sent
     preserve_clock_acquire_lock
     preserve_clock_update_deferred_clock : clock.


Lemma preserve_clock_receive_reply:
  forall i j st,
    clock (receive_reply i st j) = clock (st j).
Proof. intros. unfold receive_reply. destruct_id_dec; 
       try autorewrite with clock; reflexivity.
Qed.

Lemma preserve_clock_receive_request:
  forall i j st,
    clock (receive_request i st j) = clock (st j).
Proof. 
  intros. unfold receive_request. destruct_id_dec; molotok. 
Qed.

Lemma preserve_prop_send_request:
  forall (A:Type) (P : PState -> A) a i i_clk j st,
    a <> j 
    -> P (send_request a i i_clk st j) = P (st j).
Proof. molotok. Qed.

Lemma preserve_prop_nl_send_next_request:
  forall (A:Type) (P : PState -> A) i j j_clk l st,
    ~ In i l 
    -> P (send_next_request l j j_clk st i) = P (st i).
Proof.
  induction l.
  simpl.
  reflexivity.
  intros.
  simpl.
  simpl in H. unfold not in H. 
  destruct (id_dec a i). subst. intuition.
  rewrite IHl.
  rewrite preserve_prop_send_request. reflexivity. 
  assumption.
  intuition.
Qed.

Hint Rewrite 
     preserve_clock_receive_request
     preserve_clock_receive_reply : clock.

(*
Lemma preserve_clock_send_all_requests:
  forall i j st,
    clock (send_all_requests j st i) = clock (st i).
Proof. intros. unfold send_all_requests. autorewrite with clock.
       reflexivity. Qed.*)

Lemma preserve_clock_update_deferred:
  forall l s i j, clock (update_deferred l i s j) = clock (s j).
Proof. molotok. Qed.

Lemma preserve_clock_update_clock_self:
  forall c s i, clock (update_clock c i s i) = c.
Proof. intros. unfold update_clock.
       rewrite update_get_self_eq. simpl. reflexivity. Qed.

Lemma preserve_clock_push_deferred:
  forall i j k s c clk,
    clock (push_deferred (j, c, clk) k s i) = clock (s i).
Proof. intros. molotokdb clockdb. Qed.

Lemma clock_update_clock_eq:
  forall c i j s, 
    i <> j 
    -> clock (update_clock c i s j) = clock (s j).
Proof. intros. molotokdb clockdb. unfold update. rewrite ite_id_dec_false.
       reflexivity. assumption.
Qed.

Hint Rewrite 
     preserve_clock_update_clock_self
     preserve_clock_push_deferred
     preserve_clock_update_deferred : clock.

Lemma preserve_clock_unlock_other:
  forall i j s, i <> j -> clock (unlock i s j) = clock (s j).
Proof. intros. unfold unlock. autorewrite with clock. reflexivity. Qed.

(*
Hint Rewrite 
     preserve_clock_send_all_requests : clock.*)

Hint Rewrite
     clock_update_clock_eq
     preserve_clock_unlock_other : clockne.


(***********************)
(* fsm_state           *)
(***********************)
Lemma preserve_fsm_state_receive_request:
  forall i j s,
    fsm_state (receive_request i s j) = fsm_state (s j).
Proof. molotok. Qed.

Lemma preserve_fsm_state_update_received_from:
  forall i j s l,
    fsm_state (update_received_from l i s j) = fsm_state (s j).
Proof. molotok. Qed.

Lemma preserve_fsm_state_update_deferred_clock:
  forall i j s c,
    fsm_state (update_deferred_clock c i s j) = fsm_state (s j).
Proof. molotok. Qed.

Lemma preserve_fsm_state_update_reply_sent:
  forall r i j s,
    fsm_state (update_reply_sent r i s j) = fsm_state (s j).
Proof. molotok. Qed.

Lemma fsm_state_update_fsm_state_eq:
  forall i s ins,
    fsm_state (update_fsm_state ins i s i) = ins.
Proof. molotok. Qed.

Lemma preserve_fsm_state_update_await_from:
  forall i j l s, fsm_state (update_await_from l i s j) = fsm_state (s j).
Proof. molotok. Qed.

Lemma  preserve_fsm_state_remove_msg:
  forall i j s, fsm_state (remove_msg i s j) = fsm_state (s j).
Proof. molotok. Qed.

Lemma preserve_fsm_state_update_clock:
  forall i j c st,
    fsm_state (update_clock c i st j) = fsm_state (st j).
Proof. molotok. Qed.

Lemma preserve_fsm_state_send_next_reply:
  forall l i j i_clk st,
    fsm_state (send_next_reply l i i_clk st j) = fsm_state (st j).
Proof. induction l. simpl; reflexivity.
       intros. simpl.
       destruct a ; destruct p. destruct (id_dec i0 i).
       reflexivity. rewrite IHl. molotok.
Qed.

Lemma preserve_fsm_state_send_next_request:
  forall l i j i_clk st,
    fsm_state (send_next_request l i i_clk st j) = fsm_state (st j).
Proof. induction l. simpl; reflexivity.
       intros. simpl. rewrite IHl. molotok.
Qed.

Lemma preserve_fsm_state_send_reply:
  forall i j k i_clk j_clk st,
    fsm_state (send_reply i i_clk j j_clk st k) = fsm_state (st k).
Proof.  molotok. Qed.

Lemma preserve_fsm_state_update_deferred:
  forall d i j st,
    fsm_state (update_deferred d i st j) = fsm_state (st j).
Proof. molotok. Qed.

Lemma fsm_state_acquire_lock_self:
  forall i st,
    fsm_state (acquire_lock i st i) = Locked.
Proof. molotok. Qed.

Lemma preserve_fsm_state_push_deferred:
  forall p i st j,
    fsm_state (push_deferred p i st j) = fsm_state (st j).
Proof. molotok. Qed.
  
Hint Rewrite 
     preserve_fsm_state_send_next_reply
     preserve_fsm_state_send_next_request
     preserve_fsm_state_receive_request
     preserve_fsm_state_update_deferred_clock
     preserve_fsm_state_update_received_from
     preserve_fsm_state_remove_msg
     fsm_state_update_fsm_state_eq
     preserve_fsm_state_update_await_from
     preserve_fsm_state_update_clock
     fsm_state_acquire_lock_self
     preserve_fsm_state_update_reply_sent
     preserve_fsm_state_update_deferred
     preserve_fsm_state_send_reply
     preserve_fsm_state_push_deferred
      : fsmrw.

Lemma fsm_state_unlock_self:
  forall i s,
    fsm_state (unlock i s i) = Initial.
Proof. intros. unfold unlock. autorewrite with fsmrw. reflexivity. Qed.

Hint Rewrite
     fsm_state_unlock_self : fsmrw.

Lemma fsm_state_update_fsm_state_other:
  forall i j s ins,
    i <> j 
    -> fsm_state (update_fsm_state ins i s j) = fsm_state (s j).
Proof. molotok. Qed.

Lemma fsm_state_acquire_lock_other:
  forall i j st,
    i <> j
    -> fsm_state (acquire_lock i st j) = fsm_state (st j).
Proof. molotok. Qed.

Lemma fsm_state_receive_reply_other:
  forall i j s, i <> j -> fsm_state (receive_reply i s j) = fsm_state (s j).
Proof. intros. unfold receive_reply.  destruct_id_dec; try autorewrite with fsmrw; try reflexivity.
       rewrite fsm_state_acquire_lock_other. autorewrite with fsmrw. reflexivity.
       assumption.
Qed.

Lemma fsm_state_unlock_other:
  forall i j s, i <> j -> fsm_state (unlock i s j) = fsm_state (s j).
Proof. intros. unfold unlock. rewrite preserve_fsm_state_update_reply_sent.
       autorewrite with fsmrw. rewrite fsm_state_update_fsm_state_other.
       autorewrite with fsmrw. reflexivity. assumption.
Qed.

Lemma fsm_state_send_all_requests_other:
  forall i j s,
    i <> j -> fsm_state (send_all_requests i s j) = fsm_state (s j).
Proof.
  intros.
  timeout 5 molotokdb fsmdb. timeout 5 molotok.
  rewrite preserve_fsm_state_send_next_request.
  reflexivity.
Qed.

Hint Rewrite 
     fsm_state_receive_reply_other
     fsm_state_unlock_other
     fsm_state_send_all_requests_other
     fsm_state_acquire_lock_other
     fsm_state_update_fsm_state_other : fsmnerw.


(******************)
(* received_from  *)
(******************)
Lemma preserve_rf_update_reply_sent:
  forall r i j s,
    received_from (update_reply_sent r i s j) = received_from (s j).
Proof. molotok. Qed.

Lemma preserve_rf_send_reply:
  forall i j k c' c s,
    received_from (send_reply i c j c' s k) = received_from (s k).
Proof. molotok. Qed.

Lemma preserve_rf_remove_msg:
  forall i j s,
    received_from (remove_msg i s j) = received_from (s j).
Proof. molotok. Qed.

Lemma preserve_rf_update_deferred_clock:
  forall i j s c, received_from (update_deferred_clock c i s j) = received_from (s j).
Proof. molotok. Qed.

Lemma preserve_rf_push_deferred:
  forall i j k s,
    received_from (push_deferred i j s k) = received_from (s k).
Proof. molotok. Qed.

Lemma preserve_rf_receive_request:
  forall i j s,
    received_from (receive_request i s j) = received_from (s j).
Proof. molotok. Qed.

Lemma rf_update_rf_eq:
  forall i s l, received_from (update_received_from l i s i) = l.
Proof. molotok. Qed.

Lemma preserve_rf_update_await_from:
  forall l i j s,
    received_from (update_await_from l i s j) = received_from (s j).
Proof. molotok. Qed.

Lemma preserve_rf_update_fsm_state:
  forall inst i j s,
    received_from (update_fsm_state inst i s j) = received_from (s j).
Proof. molotok. Qed.

Lemma preserve_rf_update_clock:
  forall i j c s,
    received_from (update_clock c i s j) = received_from (s j).
Proof. molotok. Qed.

Lemma preserve_rf_update_deferred:
  forall i j s l, received_from (update_deferred l i s j) = received_from (s j).
Proof. molotok. Qed.

Lemma preserve_rf_update_reply:
  forall i j l st,
    received_from (update_reply_sent l i st j) = received_from (st j).
Proof. molotok. Qed.

Lemma preserve_rf_send_next_reply:
  forall l i i_clk j st,
    received_from (send_next_reply l i i_clk st j) = received_from (st j).
Proof. 
  induction l.
  simpl.
  reflexivity.
  intros.
  simpl.
  destruct a; destruct p.
  destruct (id_dec i0 i). reflexivity.
  rewrite IHl. molotok.
Qed.

Lemma preserve_rf_send_next_request:
  forall l i i_clk j st,
    received_from (send_next_request l i i_clk st j) = received_from (st j).
Proof.
  induction l.
  simpl.
  reflexivity.
  intros.
  simpl.    
  rewrite IHl. molotok.
Qed.

Lemma preserve_rf_acquire_lock:
  forall i j st,
    received_from (acquire_lock i st j) = received_from (st j).
Proof. molotok. Qed.

Hint Rewrite 
     preserve_rf_remove_msg
     preserve_rf_update_deferred_clock
     preserve_rf_send_reply
     preserve_rf_push_deferred
     preserve_rf_update_reply_sent 
     preserve_rf_receive_request
     preserve_rf_send_next_reply
     preserve_rf_update_reply     
     preserve_rf_update_deferred
     preserve_rf_update_clock     
     preserve_rf_update_fsm_state
     preserve_rf_acquire_lock
     preserve_rf_update_await_from
     rf_update_rf_eq
     preserve_rf_send_next_reply
     preserve_rf_send_next_request
: rfrw.

Lemma received_from_unlock_self:
  forall i st,
    received_from (unlock i st i) = [].
Proof. intros. unfold unlock. autorewrite with rfrw. reflexivity. Qed.
       
Lemma preserve_rf_send_all_requests:
  forall i j s, received_from (send_all_requests i s j) = received_from (s j).
Proof. intros. unfold send_all_requests. autorewrite with rfrw. reflexivity. Qed.

Lemma rf_update_rf_other:
  forall l i j s,
    i <> j
    -> received_from (update_received_from l i s j) = received_from (s j).
Proof. molotok. Qed.

Lemma rf_receive_reply:
  forall i j s,
    i <> j 
    -> received_from (receive_reply i s j) = received_from (s j).
Proof. intros. unfold receive_reply.  destruct_id_dec;
         try reflexivity; autorewrite with rfrw;
         try rewrite rf_update_rf_other; 
         timeout 5 molotok.
Qed.

Lemma rf_unlock_other:
  forall i j s,
    i <> j
    -> received_from (unlock i s j) = received_from (s j).
Proof.
  intros. unfold unlock. autorewrite with rfrw.
  rewrite rf_update_rf_other. autorewrite with rfrw. reflexivity. assumption.
Qed.

Hint Rewrite
     preserve_rf_send_all_requests
     received_from_unlock_self
     : rfrw.

Hint Rewrite
     rf_update_rf_other
     rf_update_rf_eq
     rf_unlock_other : rfnerw.


(*****************)
(* await_from    *)
(*****************)
Lemma preserve_af_push_deferred:
  forall i j k s,
    await_from (push_deferred i j s k) = await_from (s k).
Proof. molotok. Qed.

Lemma preserve_af_update_deferred_clock:
  forall i j s c,
    await_from (update_deferred_clock c i s j) = await_from (s j).
Proof. molotok. Qed.

Lemma preserve_af_receive_request:
  forall i j s,
    await_from (receive_request i s j) = await_from (s j).
Proof. molotok. Qed.

Lemma preserve_af_send_reply:
  forall i j k c c' s,
    await_from (send_reply i c j c' s k) = await_from (s k).
Proof. molotok. Qed.

Lemma preserve_af_remove_msg:
  forall i j s,
    await_from (remove_msg i s j) = await_from (s j).
Proof. molotok. Qed.
  
Lemma preserve_af_update_af:
  forall l i s, 
    await_from (update_await_from l i s i) = l.
Proof. molotok. Qed.

Lemma preserve_af_update_fsm_state:
  forall l i j s, 
    await_from (update_fsm_state l i s j) = await_from (s j).
Proof. molotok. Qed.

Lemma preserve_af_update_received_from:
  forall l i j s, 
    await_from (update_received_from l i s j) = await_from (s j).
Proof. molotok. Qed.

Lemma preserve_af_update_reply_sent:
  forall r i j s,
    await_from (update_reply_sent r i s j) = await_from (s j).
Proof. molotok. Qed.

Lemma preserve_af_send_next_request:
  forall l i i_clk j st,
    await_from (send_next_request l i i_clk st j) = await_from (st j).
Proof. 
  induction l. simpl.  reflexivity.
  intros.
  simpl.
  rewrite IHl. molotok. 
Qed.

Lemma preserve_af_send_next_reply:
  forall l i i_clk j st,
    await_from (send_next_reply l i i_clk st j) = await_from (st j).
Proof. 
  induction l. simpl.  reflexivity.
  intros.
  simpl. destruct a; destruct p. destruct (id_dec i0 i). reflexivity.
  rewrite IHl. molotok. 
Qed.


Lemma preserve_af_update_clock:
  forall c i j st,
    await_from (update_clock c i st j) = await_from (st j).
Proof. molotok. Qed.

Lemma preserve_af_update_deferred:
  forall l i j st,
    await_from (update_deferred l i st j) = await_from (st j).
Proof. molotok. Qed.

Lemma preserve_af_acquire_lock:
  forall i j st,
    await_from (acquire_lock i st j) = await_from (st j).
Proof. molotok. Qed.
  
Hint Rewrite 
     preserve_af_update_reply_sent
     preserve_af_send_reply
     preserve_af_update_received_from     
     preserve_af_update_fsm_state
     preserve_af_remove_msg
     preserve_af_receive_request
     preserve_af_push_deferred
     preserve_af_send_next_request
     preserve_af_send_next_reply
     preserve_af_update_deferred_clock
     preserve_af_update_deferred     
     preserve_af_acquire_lock
     preserve_af_update_clock
     preserve_af_update_af : afrw.

Lemma af_update_af_other:
  forall i j l s,
    i <> j -> await_from (update_await_from l i s j) = await_from (s j).
Proof. molotok. Qed.

Lemma af_receive_reply_other:
  forall i j s, i <> j -> await_from (receive_reply i s j) = await_from (s j).
Proof. intros. unfold receive_reply. destruct_id_dec; try rewrite af_update_af_other;
       molotok. 
Qed. 

Lemma af_send_all_requests_other:
  forall i j s, i <> j -> await_from (send_all_requests i s j) = await_from (s j).
Proof. intros. timeout 5 molotokdb afdb. timeout 5 molotokdb afnedb. intuition.
       intuition. molotokdb afdb.
Qed.

Hint Rewrite
     af_send_all_requests_other
     af_update_af_other
     af_receive_reply_other
(*     af_unlock_other *)
: afnerw.
  

(****************************************************)
(* Mailbox lemmas                                   *)
(****************************************************)

Lemma preserve_mbox_update_received_from:
  forall i j l s,
    mbox (update_received_from l i s j) = mbox (s j).
Proof. molotok. Qed.

Lemma preserve_mbox_update_await_from:
  forall i j l s,
    mbox (update_await_from l i s j) = mbox (s j).
Proof. molotok. Qed.

Lemma preserve_mbox_update_fsm_state:
  forall ins i j s,
    mbox (update_fsm_state ins i s j) = mbox (s j).
Proof. molotok. Qed.

Lemma mbox_send_reply_self:
  forall i i_clk j_clk st,
    mbox (send_reply i i_clk i j_clk st i) = mbox (st i).
Proof. molotok. Qed.

Lemma preserve_mbox_update_deferred_clock:
  forall i j c st,
    mbox (update_deferred_clock c i st j) = mbox (st j).
Proof. molotok. Qed.

Lemma preserve_mbox_update_clock:
  forall i j c st,
    mbox (update_clock c i st j) = mbox (st j).
Proof. molotok. Qed.

Lemma preserve_mbox_update_deferred:
  forall i j l st,
    mbox (update_deferred l i st j) = mbox (st j).
Proof. molotok. Qed.

Lemma preserve_mbox_push_deferred:
  forall i j l st,
    mbox (push_deferred l i st j) = mbox (st j).
Proof. molotok. Qed.

Lemma mbox_remove_msg_eq:
  forall i s,
    mbox (remove_msg i s i) = tl (mbox (s i)).
Proof.  molotok. Qed.

Lemma preserve_mbox_send_all_requests_self:
  forall i s, mbox (send_all_requests i s i) = mbox (s i).
Proof. intros. unfold send_all_requests.
       rewrite preserve_mbox_update_await_from.
       rewrite preserve_mbox_update_clock.       
       rewrite preserve_mbox_update_fsm_state.
       apply preserve_prop_nl_send_next_request.
       apply not_in_id_known_to.
Qed.

Lemma preserve_mbox_update_reply_sent:
  forall r i s j,
    mbox (update_reply_sent r i s j) = mbox (s j).
Proof. molotok. Qed.

Lemma mbox_send_next_request_self:
  forall i i_clk st,
    mbox (send_next_request (known_to i) i i_clk st i) = 
    mbox (st i).
Proof.
  intros.
  assert (H:= not_in_id_known_to).
  apply preserve_prop_nl_send_next_request.
  apply H.
Qed.

Lemma mbox_send_next_reply_self: 
  forall l i i_clk st,
    mbox (send_next_reply l i i_clk st i) = mbox (st i).
Proof.
  induction l.
  simpl. reflexivity.
  simpl. 
  intros.
  destruct a.
  destruct p.
  destruct (id_dec i0 i).
  reflexivity.
  rewrite IHl. molotok.
Qed.

Lemma preserve_mbox_acquire_lock:
  forall i j st,
    mbox (acquire_lock i st j) = mbox (st j).
Proof. molotok. Qed.

Lemma in_msg_mbox_remove_msg:
  forall msg i s j,
    In msg (mbox (remove_msg i s j)) ->
    In msg (mbox (s j)).
Proof.
  intros.
  unfold remove_msg, update in *.
  destruct (id_dec i j); subst; simpl in *; auto using in_tail_implies.
Qed.


Hint Rewrite
     preserve_mbox_update_fsm_state
     preserve_mbox_update_await_from
     preserve_mbox_update_received_from     
     preserve_mbox_update_clock
     preserve_mbox_update_deferred
     preserve_mbox_update_deferred_clock     
     preserve_mbox_send_all_requests_self
     preserve_mbox_push_deferred     
     mbox_remove_msg_eq
     mbox_send_next_request_self
     preserve_mbox_acquire_lock
     preserve_mbox_update_reply_sent
     mbox_send_next_reply_self
     mbox_send_reply_self
     in_msg_mbox_remove_msg     
  : mboxrw.

(* Previous name: mbox_send_reply_eq *)
Lemma mbox_send_reply_dest:
  forall i i_clk j j_clk st,
    i <> j
    -> mbox (send_reply j j_clk i i_clk st j) = mbox (st j) ++ [Reply i i_clk j_clk].
Proof. molotok. Qed.

Lemma mbox_send_reply_other:
  forall i j j_clk k k_clk st,
    i <> j
    -> mbox (send_reply j j_clk k k_clk st i) = mbox (st i).
Proof. molotok. Qed.

Lemma mbox_send_request_eq:
  forall i j j_clk st,
    i <> j 
    -> mbox (send_request i j j_clk st i) = (mbox (st i)) ++ [Request j j_clk].
Proof. molotok. Qed.

Hint Rewrite
     mbox_send_reply_other
     mbox_send_reply_dest
     mbox_send_request_eq          
     : mboxnerw.

(******************)
(* acquire lock   *)
(******************)

Lemma preserve_acquire_lock_other:
  forall i j st,
    i <> j
    -> fsm_state (acquire_lock i st j) = fsm_state (st j).
Proof. molotok. Qed.


(*****************************************************************************)

Lemma send_transparent:
  forall i j msg msg' st x,
    i <> j
    -> send i msg' (send j msg st) x =
       send j msg (send i msg' st) x.
Proof. molotok. Qed.

(**********************)
(* Deferred lemmas    *)
(**********************)
Lemma preserve_deferred_send_reply:
  forall i j k i_clk j_clk st,
    deferred (send_reply i i_clk j j_clk st k) = deferred (st k).
Proof. molotok. Qed.

Lemma preserve_deferred_remove_msg:
  forall i j st,
    deferred (remove_msg i st j) = deferred (st j).
Proof. molotok. Qed.

Lemma preserve_deferred_update_deferred_clock:
  forall i j c st,
    deferred (update_deferred_clock c i st j) = deferred (st j).
Proof. molotok. Qed.

Lemma deferred_push_deferred_eq:
  forall i rq st,
    deferred (push_deferred rq i st i) = rq :: (deferred (st i)).
Proof. molotok. Qed.

Lemma preserve_deferred_update_reply_sent:
  forall i j st d,
    deferred (update_reply_sent d i st j) = deferred (st j).
Proof. molotok. Qed.

Lemma preserve_deferred_update_received_from:
  forall i j st d,
    deferred (update_received_from d i st j) = deferred (st j).
Proof. molotok. Qed.

Lemma preserve_deferred_update_fsm_state:
  forall i j st d,
    deferred (update_fsm_state d i st j) =deferred (st j).
Proof. molotok. Qed.

Lemma preserve_deferred_update_clock:
  forall i j st d,
    deferred (update_clock d i st j) =deferred (st j).
Proof. molotok. Qed.

Lemma deferred_update_deferred_self:
  forall i st d,
    deferred (update_deferred d i st i) = d.
Proof. molotok. Qed.

Lemma preserve_deferred_update_await_from:
  forall i j d st,
    deferred (update_await_from d i st j) = deferred (st j).
Proof. molotok. Qed.

Lemma preserve_deferred_send_next_reply:
  forall l i i_clk j st,
    deferred (send_next_reply l i i_clk st j) =deferred (st j).
Proof. 
  induction l. simpl. reflexivity.
  intros. simpl. destruct a; destruct p. destruct (id_dec i0 i).
  reflexivity. rewrite IHl. molotok. 
Qed.

Lemma preserve_deferred_send_next_request:
  forall l i i_clk j st,
    deferred (send_next_request l i i_clk st j) =deferred (st j).
Proof. 
  induction l. simpl. reflexivity.
  intros. simpl. rewrite IHl. molotok. 
Qed.

Lemma preserve_deferred_acquire_lock:
  forall i j st,
    deferred (acquire_lock i st j) = deferred (st j).
Proof. molotok. Qed.

Hint Rewrite
     preserve_deferred_remove_msg
     preserve_deferred_send_reply     
     deferred_push_deferred_eq     
     preserve_deferred_update_reply_sent     
     preserve_deferred_update_deferred_clock
     preserve_deferred_update_fsm_state
     preserve_deferred_update_clock
     deferred_update_deferred_self
     preserve_deferred_send_next_reply
     preserve_deferred_acquire_lock
     preserve_deferred_send_next_request     
     preserve_deferred_update_await_from
     preserve_deferred_update_received_from
: dfrw.

Lemma deferred_send_all_requests:
  forall i j st,
    deferred (send_all_requests i st j) = deferred (st j).
Proof. intros.  unfold send_all_requests.
       autorewrite with dfrw. reflexivity. Qed.

Lemma deferred_receive_reply:
  forall i j st,
    deferred (receive_reply i st j) = deferred (st j).
Proof. intros. unfold receive_reply. 
       destruct_id_dec;
       try autorewrite with dfrw; reflexivity.
Qed.

Hint Rewrite
     deferred_receive_reply
     deferred_send_all_requests
     : dfrw.

Lemma deferred_receive_request_other:
  forall i j st,
    i <> j -> deferred (receive_request i st j) = deferred (st j).
Proof. intros. unfold receive_request. destruct_id_dec; timeout 5 molotok. Qed.

Lemma deferred_update_deferred_neq:
  forall l i j st,
    i <> j ->
    deferred (update_deferred l i st j) = deferred (st j).
Proof. intros. autounfold with defs. molotok. Qed.

Hint Rewrite     
     deferred_receive_request_other
     deferred_update_deferred_neq
     : dfnerw.

Lemma deferred_unlock_non_eq:
  forall i j st,
    i <> j -> deferred (unlock i st j) = deferred (st j).
Proof. 
  intros. unfold unlock. autorewrite with dfrw.
  autorewrite with dfnerw. autorewrite with dfrw.
  reflexivity. assumption.
Qed.

Lemma deferred_push_deferred_neq:
  forall r i j st,
    i <> j -> deferred (push_deferred r i st j) = deferred (st j).
Proof. molotok. Qed.
  
Hint Rewrite     
     deferred_push_deferred_neq
     deferred_unlock_non_eq : dfnerw.



(************************)
(*Deferred clock lemmas *)
(************************)

Lemma preserve_deferred_clock_update_reply_sent:
  forall i j d st,
    deferred_clock (update_reply_sent d i st j) = deferred_clock (st j).
Proof. molotok. Qed.

Lemma preserve_deferred_clock_send_reply:
  forall i j k st c c',
    deferred_clock (send_reply i c j c' st k) = deferred_clock (st k).
Proof. molotok. Qed.

Lemma preserve_deferred_clock_remove_msg:
  forall i j st,
    deferred_clock (remove_msg i st j) = deferred_clock (st j).
Proof. molotok. Qed.

Lemma preserve_deferred_clock_update_received_from:
  forall i j d  st,
    deferred_clock (update_received_from d i st j) = deferred_clock (st j).
Proof. molotok. Qed.

Lemma preserve_deferred_clock_update_fsm_state:
  forall i j ins st,
    deferred_clock (update_fsm_state ins i st j) = deferred_clock (st j).
Proof. molotok. Qed.

Lemma preserve_deferred_clock_update_await_from:
  forall i j l st,
    deferred_clock (update_await_from l i st j) = deferred_clock (st j).
Proof. molotok. Qed.

Lemma deferred_clock_update_self:
  forall i c st,
    deferred_clock (update_deferred_clock c i st i) = c.
Proof. molotok. Qed.

Lemma deferred_clock_push_deferred:
  forall i j d st,
    deferred_clock (push_deferred d i st j) = deferred_clock (st j).
Proof. molotok. Qed.

Lemma preserve_deferred_clock_update_clock:
  forall i j c st,
    deferred_clock (update_clock c i st j) = deferred_clock (st j).
Proof. molotok. Qed.

Lemma preserve_deferred_clock_update_deferred:
  forall i j d st,
    deferred_clock (update_deferred d i st j) = deferred_clock (st j).
Proof. molotok. Qed.

Lemma deferred_clock_send_next_reply:
  forall l i i_clk j st,
   deferred_clock (send_next_reply l i i_clk st j) =deferred_clock (st j).
Proof. induction l. simpl. reflexivity.
       intros. simpl. destruct a ; destruct p. destruct (id_dec i0 i). reflexivity.
       rewrite IHl. molotok.
Qed.

Lemma deferred_clock_send_next_request:
  forall l i i_clk j st,
   deferred_clock (send_next_request l i i_clk st j) =deferred_clock (st j).
Proof. induction l. simpl. reflexivity.
       intros. simpl. rewrite IHl. molotok.
Qed.

Lemma preserve_deferred_clock_acquire_lock:
  forall i j st,
    deferred_clock (acquire_lock i st j) = deferred_clock (st j).
Proof. molotok. Qed.

Hint Rewrite
     preserve_deferred_clock_update_received_from
     preserve_deferred_clock_update_fsm_state
     preserve_deferred_clock_update_await_from     
     deferred_clock_update_self
     preserve_deferred_clock_update_reply_sent
     preserve_deferred_clock_send_reply
     preserve_deferred_clock_remove_msg
     deferred_clock_send_next_reply
     deferred_clock_send_next_request
     deferred_clock_push_deferred    
     preserve_deferred_clock_acquire_lock
     preserve_deferred_clock_update_clock
     preserve_deferred_clock_update_deferred
      : dfclkrw.

(* preserve_deferred_clock_send_all_requests *)

Lemma preserve_deferred_clock_send_all_requests:
  forall i j st,
    deferred_clock (send_all_requests i st j) = deferred_clock (st j).
Proof. intros. unfold send_all_requests.
       autorewrite with dfclkrw. reflexivity. Qed.

Hint Rewrite
     preserve_deferred_clock_send_all_requests : dfclkrw.

Lemma deferred_clock_update_neq:
  forall i j c st,
    i <> j
    -> deferred_clock (update_deferred_clock c i st j) = deferred_clock (st j).
Proof. molotok. Qed.

Lemma deferred_clock_receive_request_eq:
  forall i j st,
    i <> j
    -> deferred_clock (receive_request i st j) = deferred_clock (st j).
Proof. intros. unfold receive_request. destruct_id_dec; timeout 5 molotok. 
Qed.

Lemma deferred_clock_receive_reply_eq:
  forall i j st,
    i <> j
    -> deferred_clock (receive_reply i st j) = deferred_clock (st j).
Proof.
  intros. unfold receive_reply. destruct_id_dec. reflexivity.
  autorewrite with dfclkrw. rewrite deferred_clock_update_neq.
  autorewrite with dfclkrw. reflexivity. assumption.
  rewrite deferred_clock_update_neq. autorewrite with dfclkrw.
  reflexivity. assumption. autorewrite with dfclkrw. reflexivity. reflexivity.
Qed.

Lemma deferred_clock_unlock_eq:
  forall i j st,
    i <> j 
      -> deferred_clock (unlock i st j) = deferred_clock (st j).
Proof.
  intros. unfold unlock.
  autorewrite with dfclkrw.
  reflexivity.
Qed.

Hint Rewrite
     deferred_clock_unlock_eq
     deferred_clock_receive_reply_eq
     deferred_clock_update_neq
     deferred_clock_receive_request_eq : dfclknerw.

(************************)
(*Reply_sent lemmas     *)
(************************)
Lemma preserve_reply_sent_update_await_from:
  forall s l i j,
    reply_sent (update_await_from l i s j) = reply_sent (s j).
Proof. molotok. Qed.

Lemma preserve_reply_sent_update_fsm_state:
  forall s l i j,
    reply_sent (update_fsm_state l i s j) = reply_sent (s j).
Proof. molotok. Qed.

Lemma preserve_reply_sent_send_reply:
  forall i j k i_clk j_clk st,
    reply_sent (send_reply i i_clk j j_clk st k) = reply_sent (st k).
Proof. molotok. Qed.

Lemma preserve_reply_update_reply_sent:
  forall i r s, reply_sent (update_reply_sent r i s i) = r.
Proof. molotok. Qed.

Lemma preserve_reply_sent_update_deferred_clock:
  forall i j st c,
    reply_sent (update_deferred_clock c i st j) = reply_sent (st j).
Proof. molotok. Qed.

Lemma preserve_reply_sent_remove_msg_other:
  forall s i j,
    reply_sent (remove_msg i s j) = reply_sent (s j).
Proof. molotok. Qed.

Lemma preserve_reply_sent_push_deferred:
  forall st i j r,
    reply_sent (push_deferred r i st j) = reply_sent (st j).
Proof. molotok. Qed.

Lemma preserve_reply_sent_receive_update_clock:
  forall st i j c,
    reply_sent (update_clock c i st j) = reply_sent (st j).
Proof. molotok. Qed.   

Lemma preserve_reply_sent_receive_update_deferred:
  forall st i j d,
    reply_sent (update_deferred d i st j) =reply_sent (st j).
Proof. molotok. Qed.        

Lemma preserve_reply_sent_receive_update_received_from:
  forall st i j d,
    reply_sent (update_received_from d i st j) =reply_sent (st j).
Proof. molotok. Qed.        

Lemma preserve_reply_sent_send_next_request:
  forall l i i_clk j s,
   reply_sent (send_next_request l i i_clk s j) = reply_sent (s j).
Proof. induction l. simpl. reflexivity.
       intros. simpl. rewrite IHl. molotok . 
Qed.

Lemma preserve_reply_sent_aquire_lock:
  forall i j s,
    reply_sent (acquire_lock i s j) = reply_sent (s j).
Proof. molotok. Qed.

Hint Rewrite     
     preserve_reply_sent_update_fsm_state
     preserve_reply_sent_update_await_from
     preserve_reply_update_reply_sent
     preserve_reply_sent_remove_msg_other
     preserve_reply_sent_send_reply
     preserve_reply_sent_push_deferred
     preserve_reply_sent_update_deferred_clock    
     preserve_reply_sent_receive_update_clock
     preserve_reply_sent_receive_update_deferred
     preserve_reply_sent_receive_update_received_from     
     preserve_reply_sent_send_next_request
     preserve_reply_sent_aquire_lock
     : rsrw.

Lemma preserve_reply_sent_receive_reply:
  forall st i j ,
    reply_sent (receive_reply i st j) = reply_sent (st j).
Proof. intros. unfold receive_reply.  destruct_id_dec; (try autorewrite with rsrw; reflexivity).
Qed.

Lemma preserve_reply_sent_send_all_requests:  
  forall i j s, reply_sent (send_all_requests i s j) = reply_sent (s j).
Proof. intros. unfold send_all_requests.
       destruct_id_dec; (try autorewrite with rsrw; reflexivity).
Qed.

Lemma preserve_reply_sent_acquire_lock:
  forall i j st,
    reply_sent (acquire_lock i st j) = reply_sent (st j).
Proof. molotok. Qed.


Hint Rewrite 
     preserve_reply_sent_receive_reply
     preserve_reply_sent_send_all_requests 
     preserve_reply_sent_acquire_lock
     : rsrw.

Lemma af_send_all_requests_self:
  forall i s,
    await_from (send_all_requests i s i) = known_to i.
Proof. molotok. Qed.

Hint Rewrite
     af_send_all_requests_self
  : afrw.

Lemma preserve_reply_sent_send_next_reply:
  forall l i i_clk j s,
    i <> j 
    -> reply_sent (send_next_reply l i i_clk s j) = reply_sent (s j).
Proof. induction l. simpl. reflexivity.
       intros. simpl.  destruct a ; destruct p . destruct (id_dec i0 i).
       reflexivity.
       rewrite IHl. molotok . assumption. 
Qed.

Lemma reply_sent_update_reply_sent_eq:
  forall r s i j, 
    i <> j 
    -> reply_sent (update_reply_sent r i s j) = reply_sent (s j).
Proof. molotok. Qed.

Lemma reply_sent_send_reply_neq:
  forall j j_clk i_clk i st j0,
    i <> j0 
    -> reply_sent (send_reply j j_clk i i_clk st j0) = reply_sent (st j0).
Proof. molotok. Qed.

Hint Rewrite
     preserve_reply_sent_send_next_reply
     reply_sent_update_reply_sent_eq
     reply_sent_send_reply_neq
     : rsnerw.