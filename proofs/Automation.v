Require Import Init Bool NPeano List StepStarTheory Arith Program Fin .
Require Import JRWTactics System.
Import ListNotations.

Ltac destruct_id_dec :=
  repeat 
    match goal with
      | [ |- context[match ?X with _ => _ end]] => destruct X eqn:?; try subst
      | [ |- context[if ?X then _ else _]] => destruct X eqn:?; try subst
      | [ H : context[if ?X then _ else _] |- _] => destruct X eqn:?; try subst
      | [ H : context[match ?X with _ => _ end] |- _] => destruct X eqn:?; try subst
    end. 

Ltac molotok :=
  intros; repeat (autounfold with defs);
  destruct_id_dec; simpl; try reflexivity; try intuition.

Definition clockdb       := 0.
(* Definition lockeddb      := 1.  *)
(* Definition lockednedb    := 2. *)
Definition fsmdb         := 1.
Definition fsmnedb       := 2.
Definition instrdb       := 3. 
Definition instrnedb     := 4.
Definition rfdb          := 5.
Definition rfnedb        := 6.
Definition afdb          := 7.
Definition afnedb        := 8.
Definition defdb         := 9.
Definition defnedb       := 10.
Definition defclkdb      := 11.
Definition defclknedb    := 12.
Definition mboxdb        := 13.
Definition mboxnedb      := 14.
Definition rsdb          := 15.
Definition rsnedb        := 16.

Ltac molotokdb db :=    
    autounfold with defs; 
    destruct_id_dec; 
    simpl;
    match db with 
      | clockdb => try autorewrite with clock
      | fsmdb => try autorewrite with fsmrw
      | fsmnedb => try autorewrite with fsmnerw
      | instrdb => try autorewrite with instrrw
      | instrnedb => try autorewrite with instrnerw
      | rfdb => try autorewrite with rfrw
      | rfnedb => try autorewrite with rfnerw
      | afdb => try autorewrite with afrw
      | afnedb => try autorewrite with afnerw
      | defdb => try autorewrite with dfrw
      | defnedb => try autorewrite with dfnerw
      | defclkdb => try autorewrite with dfclkrw
      | defclknedb => try autorewrite with dfclknerw
      | mboxdb => try autorewrite with mboxrw
      | mboxnedb => try autorewrite with mboxnerw
      | rsdb => try autorewrite with rsrw
      | rsnedb => try autorewrite with rsnerw
      | _ => fail
    end;
    simpl; try reflexivity.

Ltac inv_step :=
  match goal with
      [ H : step _ _ _ |- _ ] => invcs H
  end.

Ltac in_empty_list_contra:=
  match goal with
    | [ H1 : ?X = [], H2 : In ?Y ?X |- _] =>
      rewrite H1 in H2; simpl in H2; contradiction
    | [ H1 : [] = ?X, H2 : In ?Y ?X |- _] =>
      rewrite <- H1 in H2; simpl in H2; contradiction
  end.

Ltac find_discriminate :=
  match goal with
    | [ H : ?X = _ |- ?X <> _ ] => rewrite H; try discriminate
  end.

Lemma hd_in_implies:
  forall {A:Type} l (eq_dec: forall (x y:A), {x = y} + {x <> y}) (e:A),
    hd_error l = Some e -> In e l.
Proof.
  induction l.
  simpl. intros. inversion H.
  intros.
  destruct (eq_dec a e). subst a.
  simpl. left. reflexivity.
  simpl in H. inversion H. apply n in H1. contradiction.
Qed.

Lemma in_tail_implies:
  forall {A:Type} (l:list A) (eq_dec: forall (x y:A), {x = y} + {x <> y}) x,
    In x (tl l) 
    -> In x l.
Proof.
  induction l.
  simpl. intuition.
  intros.
  destruct (eq_dec a x).
  subst a.
  simpl. left. reflexivity.
  simpl in H. simpl. right. assumption.
Qed.

Ltac inv_In_hyp :=
    match goal with
    | [H: In _ _ |- _] => invcs H
    end.

Ltac in_tail_implies :=
  find_apply_lem_hyp in_tail_implies.

