%% =============================================================================
%% @author 
%%   Evgeniy Shishkin <evgeniy.shishkin@gmail.com>
%% @doc
%%   Ricart-Agrawala distributed mutual exclusion protocol
%%   extended with fault tolerance feauture and dynamic
%%   participants join/leave scenario support.
%% =============================================================================

-module(ra_mutex_ft).
-compile(export_all).

%% Client API
-export([start/0, stop/0, lock/0, unlock/0]).

-type fsm_state() :: 
        wait_start_protocol
      |	initial
      | process_messages 
      | locked
      | stopped.

-record(request, {src, clock}).
-record(reply, {src}).
-define(clientdown(From), {'DOWN', _, process, From, _Info}).
-record(clientup, {src}).

-record(state, {id :: integer(), 
		await_from :: list(integer()),
		clock :: integer(), 
		deferred :: list(integer()),
		deferred_clock :: integer(),
		clients :: list(term()), 
		fsm_state :: fsm_state(),
		lock_pid :: pid()}).

-define(GROUP, ?MODULE).
-define(ASSERT(X), true = X).
-define(LOG(Text,Args), 
	io:format("{~p,~p,~p}: ~s~n",
		  [node(),?MODULE,?LINE,io_lib:format(Text,Args)])).
%% =============================================================================
%% Start ra_mutex_ft process 
%% Args: list of clients known at the start time
%% ============================================================================= 
start() ->
    case lists:member(?MODULE, erlang:registered()) of
	true -> 
	    {ok, erlang:whereis(?MODULE)};
	false -> 
	    Pid = erlang:spawn_link(?MODULE, start_protocol, [self()]),
	    receive 
		registered -> {ok, Pid}
	    end
    end.

proc_rpc(Request) ->
    Pid = erlang:whereis(?MODULE),
    case Pid of
	undefined -> 
	    {error, not_running};
	_ -> 
	    erlang:send(Pid, Request),
	    receive
		Reply -> Reply
	    end
    end.    

stop() -> proc_rpc({stop, self()}).
lock() -> proc_rpc({lock, self()}).
unlock() -> proc_rpc({unlock, self()}).

%% =============================================================================
%% Main control logic
%% =============================================================================
start_protocol(ClientPid) ->
    % Interleaving is possible between our name registration
    % and #clientup delivery. 
    erlang:register(?MODULE, self()),
    pg2:create(?GROUP),                  % idemponent call
    pg2:join(?GROUP, self()),            % add ourself into the group
    % WARNING:
    % We rely on assumption that a node is working in FAIL-STOP model, so if
    % it fails it either never runs again or it runs with a new identifier.
    erlang:send(ClientPid, registered),
    main(init_state(self(), pg2:get_members(?GROUP) -- [self()])).

stop_protocol() ->
    pg2:leave(?GROUP, self()).

init_state(Id, Clients) ->
    #state{id = Id, clock = 0, deferred = [], 
	   deferred_clock = 0, clients = Clients,
	   fsm_state = wait_start_protocol,
	   await_from = [], lock_pid = undefined}.

send_message(ProcId, Msg) ->
    erlang:send(ProcId, Msg).
send_reply(Dst, Src) ->
    send_message(Dst, #reply{src=Src}).
send_request(Dst, Src, Clock) ->
    send_message(Dst, #request{src=Src, clock=Clock}).
broadcast(Clients, Msg) ->
    [send_message(P, Msg) || P <- Clients].

add_monitor_member(Id, State) ->
    erlang:monitor(process, Id),
    State#state{clients=lists:usort(State#state.clients ++ [Id])}.
remove_member(Id, State) ->
    Clients = lists:usort(State#state.clients -- [Id]),
    AwaitFrom = lists:usort(State#state.await_from -- [Id]),
    State#state{clients=Clients, await_from=AwaitFrom}.

monitor_clients(Clients) ->
    [erlang:monitor(process, C) || C <- Clients].

%% ===========================================================================
%% Process enters into and executes this function.
%% ===========================================================================
main(State) ->
    case State#state.fsm_state of
	wait_start_protocol ->
	    main(wait_start_protocol(State));
	initial -> 
	    main(initial(State));
	process_messages ->
	    main(process_messages(State));
	locked ->
	    main(locked(State));
	stopped ->
	    stop_protocol()
    end.

wait_start_protocol(State) ->
    ?LOG("wait_start_protocol ~p", [State#state.id]),
    % Monitor request signal must be sent before clientup?
    monitor_clients(State#state.clients),
    broadcast(State#state.clients, #clientup{src=State#state.id}),
    State#state{fsm_state=initial}.
    
initial(State) ->
    ?LOG("initial ~p", [State#state.id]),
    receive	
	?clientdown(HisId) ->
	    remove_member(HisId, State);
	#clientup{src=HisId} ->
	    add_monitor_member(HisId, State);
	#request{src=HisId, clock=HisClock} ->
	    ?ASSERT(lists:member(HisId, State#state.clients)),
	    DClock = max(HisClock, State#state.deferred_clock),
	    send_reply(HisId, State#state.id),
	    State#state{deferred_clock=DClock};
	{lock, Pid} ->
	    send_all_requests(State#state{lock_pid=Pid});
	{unlock, Pid} ->
	    erlang:send(Pid, not_locked),
	    State;
	{stop, Pid} ->
	    State#state{fsm_state=stopped}
    end.

%% ===========================================================================
%% Broadcast a request for a lock; supply each request with our CLOCK
%% =========================================================================== 
send_all_requests(State) ->
    Clock = 1 + State#state.deferred_clock,
    Clients = State#state.clients, 
    broadcast(Clients, #request{src=State#state.id, clock=Clock}),
    State#state{fsm_state=process_messages, await_from=Clients, clock=Clock}.

critical_section(State) ->
    % Do some thing with exclusive resource here
    ?LOG("Client ~p acquired the lock.~n", [State#state.id]),
    erlang:send(State#state.lock_pid, locked),
    State#state{fsm_state=locked}.

%% ===========================================================================
%% Process incoming messages
%%  1) process replies from other processes - from those who gave their permission 
%%     to acquire the lock for us
%%  2) process requests from clients - we are going to give our permission only 
%%     if {HisId,HisClock} < {MyId,MyClock}
%%             otherwise put the request into defered list. 
%% If and only if we received all replies we need only then we execute critical
%% section block.
%% ===========================================================================
process_messages(State) when State#state.await_from =:= [] ->
    ?LOG("process_messages ~p", [State#state.id]),
    critical_section(State);

process_messages(State) ->
    ?LOG("process_messages ~p", [State#state.id]),
    MyClock = State#state.clock,
    MyId = State#state.id,
    receive
	?clientdown(HisId) ->
	    ?LOG("clientdown ~p", [HisId]),
	    remove_member(HisId, State);
	#clientup{src=HisId} ->
	    ?LOG("clientup ~p", [HisId]),
	    IsMember = lists:member(HisId, State#state.clients),
	    % We should not send a request to the already known process 
	    case IsMember of 
		true -> State;
		false ->
		    State1 = add_monitor_member(HisId, State),
		    send_request(HisId, MyId, MyClock),
		    State1
	    end;
	#reply{src=HisId} ->
	    ?LOG("reply from ~p", [HisId]),
            AwaitFrom1 = lists:delete(HisId,State#state.await_from),	  
	    State#state{await_from=AwaitFrom1};
	#request{src=HisId, clock=HisClock} 
	  when {HisClock, HisId} < {MyClock, MyId} ->    
	    ?LOG("request ~p from ~p", [HisClock, HisId]),
	    ?ASSERT(lists:member(HisId, State#state.clients)),
	    send_reply(HisId, MyId),
	    State;
	#request{src=HisId, clock=HisClock} ->
	    ?LOG("request ~p from ~p", [HisClock, HisId]),
	    ?ASSERT(lists:member(HisId, State#state.clients)),
	    DClock = State#state.deferred_clock,
	    Deferred = State#state.deferred,
	    DClock1 = max(DClock, HisClock),
	    State#state{deferred_clock=DClock1, 
			deferred=[HisId|Deferred]}	   
    end.

%% ===========================================================================
%% Unlock phase - send our reply to all deferred requests, so they
%% are able to acquire the lock after us.
%% ===========================================================================
locked(State) ->
    % ?LOG("locked ~p", [State#state.id]),
    receive
	?clientdown(HisId) ->
	    ?LOG("clientdown ~p", [HisId]),
	    remove_member(HisId, State);
	#clientup{src=HisId} ->
	    ?LOG("clientup ~p", [HisId]),	    
	    add_monitor_member(HisId, State);
	{unlock, Pid} when State#state.lock_pid == Pid ->
	    unlock(State);
	{unlock, Pid} ->
	    erlang:send(Pid, lock_busy),
	    State;
	{lock, Pid} when State#state.lock_pid == Pid ->
	    erlang:send(Pid, already_locked),
	    State;
	{lock, Pid} ->
	    erlang:send(Pid, lock_busy),
	    State
    end.
	    
unlock(State) ->
    ?LOG("Client ~p released the lock.~n", [State#state.id]),
    MyId = State#state.id,
    broadcast(State#state.deferred, #reply{src=MyId}),
    erlang:send(State#state.lock_pid, unlocked),
    State#state{deferred = [], fsm_state = initial, lock_pid = undefined}.
