(******************************************************************************)
(* Description:                                                               *)
(*     Formal model for RA distributed mutex algorithm without                *)
(*     failure/join scenarious;                                               *)
(*   Author:                                                                  *)
(*     Evgeniy Shishkin <Evgeniy.Shishkin@gmail.com>                          *)
(*   Notes:                                                                   *)
(*     See ra_mutex.erl for detailed algorithm implementation in Erlang       *)
(*     with useful comments.                                                  *)
(******************************************************************************)

Require Import Init Bool NPeano List StepStarTheory Arith Program Fin .
Require Import JRWTactics Clients System Invariants Automation Preservations.
Import ListNotations.

Set Printing Dependent Evars Line. (* needed for proof-tree to work properly *)
Hint Resolve eq_msg_dec id_dec.

(***************************************************************)
(* We call the pair of (clock, id) as virtual clock aka vclock *)
(***************************************************************)
Definition lt_vclock i i_clk j j_clk :=
  i_clk < j_clk 
     \/ (i_clk = j_clk /\ id_lt i j).

Hint Resolve lt_irrefl.

Lemma in_mbox_reply_send_next_request:
  forall l j i j_clk i_clk i_clk' k st,
    In (Reply j j_clk i_clk) 
       (mbox (send_next_request l i i_clk' st k)) 
    -> In (Reply j j_clk i_clk) (mbox (st k)).
Proof.
  induction l as [| IH].
  simpl. intros. assumption.
  intros.
  simpl in *.
  eapply IHl in H.
  unfold send_request in *.
  destruct_id_dec. assumption.
  unfold send,update in *; destruct_id_dec.
  simpl in *.
  apply in_app_or in H. break_or_hyp.
  assumption. inversion H0.
  inversion H. inversion H.
  assumption.
Qed.

Lemma mbox_send_next_reply_in_df:
  forall i i_clk i0 i0_clk l newClock st,
    i <> i0 
    -> In (Reply i i_clk i0_clk) (mbox (send_next_reply l i newClock st i0))  
    -> In (Reply i i_clk i0_clk) (mbox (st i0)) \/ 
       (exists i_clk', In (i0,i0_clk,i_clk') l 
                       /\ i_clk = newClock).
Proof.
  induction l.
  simpl. intros. left. assumption.
  intros.
  simpl in H0.
  destruct a.
  destruct p.
  destruct (id_dec i1 i).
  subst i1.
  left. assumption.
  unfold send_reply, send in H0.
  rewrite ite_id_dec_false in H0.
  destruct (id_dec i1 i0).
  subst i1.  
  apply IHl in H0.
  inversion H0.
  unfold send,update in H1.
  rewrite ite_id_dec_true in H1.
  simpl in H1.
  apply in_app_or in H1.
  inversion H1.
  left. assumption.
  simpl in H2.
  inversion H2. inversion H3.
  subst c0.
  right. exists c.
  simpl. split. left. reflexivity. reflexivity.
  contradiction.
  inversion H1. 
  right.
  exists x.
  simpl. split. right. inversion H2. assumption.
  inversion H2. assumption.
  assumption.  
  eapply IHl in H0.
  inversion H0. left. 
  unfold send,update in H1. rewrite ite_id_dec_false in H1.
  assumption. assumption.
  right.
  inversion H1.
  exists x.
  simpl. inversion H2; clear H2. split. right. assumption.
  assumption.
  assumption.
  auto.
Qed.

Lemma mbox_send_next_reply_diff:
  forall l i j j_clk i0 i_clk newClock st,
    In (Reply j j_clk i_clk) 
       (mbox (send_next_reply l i newClock st i0))
    -> j <> i
    -> In (Reply j j_clk i_clk) (mbox (st i0)).
Proof.
  induction l.
  simpl. intros. assumption.
  intros.
  simpl in H.
  destruct a. destruct p.
  destruct (id_dec i1 i).
  subst i1.
  assumption.
  unfold send_reply,send in H.
  rewrite ite_id_dec_false in H.
  destruct (id_dec i1 i0).
  subst i1.
  apply IHl in H.
  unfold send,update in H.
  rewrite ite_id_dec_true in H.
  simpl in H.
  apply in_app_or in H.
  inversion H.
  assumption.
  inversion H1. inversion H2.
  symmetry in H4. apply H0 in H4. contradiction.
  inversion H2.
  assumption.
  apply IHl in H.
  unfold send,update in H.
  rewrite ite_id_dec_false in H.
  assumption.
  assumption.
  assumption.
  auto.
Qed.

Lemma remove_first_eq_or:
  forall l (i1:ID),   
    remove_first id_dec i1 l = [] -> l = [] \/ l = [i1].
Proof. 
  intros.
  destruct l.
  left. reflexivity.
  right.
  simpl in H.
  destruct (id_dec f i1).
  subst.  reflexivity.
  simpl in H. symmetry in H.
  apply nil_cons in H. contradiction.
Qed.
  
Definition filt_fun :=
  fun (j:ReplySig) =>
    let '(hisId,hisClock,myClock) := j in
    hisId.
    
Definition get_id := map filt_fun.

Lemma in_app_right:
  forall {A:Type} j (l : list A) l1, In j l -> In j (l1 ++ l).
Proof. intros. apply in_or_app. right. assumption. Qed.

Definition rf_plus_af_equ_inv (st: GState) :=
  forall i j,
    fsm_state (st i) <> Initial
    -> In j (known_to i) 
    -> In j (get_id (received_from (st i)) ++ await_from (st i)).

Lemma rf_plus_af_equ_inv_lock_step:
  forall i s s',
    rf_plus_af_equ_inv s
    -> step s (Lock i) s'
    -> rf_plus_af_equ_inv s'.
Proof.
  unfold rf_plus_af_equ_inv.
  intros i s s' H H0 i0 j H2 H3.  
  inversion H0 ; subst ; clear H0.
  (* known_to i <> [] *)
  destruct (id_dec i0 i).
  subst i0.
  rewrite af_send_all_requests_self. auto using in_app_right.
  autorewrite with rfrw. rewrite af_send_all_requests_other.
  find_neq_erewrite_lem' fsm_state_send_all_requests_other.
  auto. auto.
Qed.

Ltac smart_rewrite :=
  repeat
    match goal with
    | [H: context [fsm_state] |- _] =>
      progress (autorewrite with fsmrw in H)
    | [|- context [fsm_state]] =>
      progress (autorewrite with fsmrw)
    | [H: context [received_from] |- _] =>
      progress (autorewrite with rfrw in H)
    | [|- context [received_from]] =>
      progress (autorewrite with rfrw)
    | [H: context [reply_sent] |- _] =>
      progress (autorewrite with rsrw in H)
    | [|- context [reply_sent]] =>
      progress (autorewrite with rsrw)               
    | [H: context [await_from] |- _] =>
      progress (autorewrite with afrw in H)
    | [|- context [await_from]] =>
      progress (autorewrite with afrw)
    | [H: context [mbox] |- _] =>
      progress (autorewrite with mboxrw in H)
    | [|- context [mbox]] =>
      progress (autorewrite with mboxrw)
    | [H: context [clock] |- _] =>
      progress (autorewrite with clock in H)               
    | [|- context [clock]] =>
      progress (autorewrite with clock)
    | [H: context [deferred] |- _] =>
      progress (autorewrite with dfrw in H)               
    | [|- context [deferred]] =>
      progress (autorewrite with dfrw)
    | [H: context [deferred_clock] |- _] =>
      progress (autorewrite with dfclkrw in H)               
    | [|- context [deferred_clock]] =>
      progress (autorewrite with dfclkrw)
    | _ => fail
    end.

Ltac two_branches :=
  match goal with
    (* received_from *)
    | [H: context[received_from (update_received_from _ ?X _ ?Y)] |- _] =>
        (*idtac H;*) destruct (id_dec X Y); subst;
        [ autorewrite with rfrw in H | autorewrite with rfnerw in H ]
    | [|- context[received_from (update_received_from _ ?X _ ?Y)]] =>
        (*idtac H;*) destruct (id_dec X Y); subst;
        [ autorewrite with rfrw | autorewrite with rfnerw ]          
    | [H: context[reply_sent(update_reply_sent _ ?X _ ?Y)] |- _] =>
       destruct (id_dec X Y); subst;
       [ autorewrite with rsrw in H | autorewrite with rsnerw in H ]
    (* reply_sent *)
    | [|- context[reply_sent(update_reply_sent _ ?X _ ?Y)]] =>
       destruct (id_dec X Y); subst;
       [ autorewrite with rsrw | autorewrite with rsnerw]
    | [|- context[reply_sent((send_reply ?X _ ?Y _ _) ?Z)]] =>
       destruct (id_dec X Z); subst;
       [ autorewrite with rsrw | autorewrite with rsnerw]         
    | [H: context[reply_sent((send_reply ?X _ ?Y _ _) ?Z)] |- _] =>
       destruct (id_dec X Z); subst;
       [ autorewrite with rsrw in H | autorewrite with rsnerw in H]
    (* deferred_clock *)         
    | [|- context[deferred_clock (update_deferred_clock _ ?X _ ?Y)]] =>
      destruct (id_dec X Y); subst;
      [ autorewrite with dfclkrw | autorewrite with dfclknerw]
    | [H: context[deferred_clock (update_deferred_clock _ ?X _ ?Y)] |- _] =>
      destruct (id_dec X Y); subst;
      [ autorewrite with dfclkrw | autorewrite with dfclknerw]
    (* deferred *)
    | [H: context[deferred (push_deferred _ ?X _ ?Y)] |- _] =>
      destruct (id_dec X Y); subst;
      [ autorewrite with dfrw | autorewrite with dfnerw]
    | [H: context[deferred (update_deferred _ ?X _ ?Y)] |- _] =>
      destruct (id_dec X Y); subst;
      [ autorewrite with dfrw | autorewrite with dfnerw]
    | [|- context[deferred (push_deferred _ ?X _ ?Y)]] =>
      destruct (id_dec X Y); subst;
      [ autorewrite with dfrw in * | autorewrite with dfnerw in *]
    | [|- context[deferred (update_deferred _ ?X _ ?Y)] ] =>
      destruct (id_dec X Y); subst;
      [ autorewrite with dfrw | autorewrite with dfnerw]        
        (*clock*)
    | [H: context[clock (update_clock _ ?X _ ?Y)] |- _] =>
      destruct (id_dec X Y); subst;
      [ autorewrite with clock | autorewrite with clockne]
    | [|- context[clock (update_clock _ ?X _ ?Y)]] =>
      destruct (id_dec X Y); subst;
      [ autorewrite with clock in * | autorewrite with clockne]
    (*fsm_state*)
    | [H: context[fsm_state (update_fsm_state _ ?X _ ?Y)] |- _] =>
      destruct (id_dec X Y); subst;
      [ autorewrite with fsmrw in * | autorewrite with fsmnerw in *]
    | [|- context[fsm_state (update_fsm_state _ ?X _ ?Y)]] =>
      destruct (id_dec X Y); subst;
      [ autorewrite with fsmrw in * | autorewrite with fsmnerw in *]                
    | _ =>
      fail
  end.

Ltac neq_branch :=
  match goal with
    | [H: ?X <> ?Y |- context[clock (update_clock _ ?X _ ?Y)]] =>
      rewrite clock_update_clock_eq
    | [H: ?Y <> ?X |- context[clock (update_clock _ ?X _ ?Y)]] =>
      rewrite clock_update_clock_eq                  
    | [H: ?X <> ?Y |- context[reply_sent(send_next_reply _ ?X _  _ ?Y)]] =>
      autorewrite with rsnerw
    | [H: ?Y <> ?X |- context[reply_sent(send_next_reply _ ?X _  _ ?Y)]] =>
      autorewrite with rsnerw
    | [H: ?X <> ?Y |- context[reply_sent(update_reply_sent _ ?X _ ?Y)]] =>
      autorewrite with rsnerw
    | [H: ?Y <> ?X|- context[reply_sent(update_reply_sent _ ?X _ ?Y)]] =>
      autorewrite with rsnerw
  end.
  
Lemma rf_plus_af_equ_recv_request_step:
  forall i i0 c s s',
    rf_plus_af_equ_inv s
    -> step s (ReceiveRequest i i0 c) s'
    -> rf_plus_af_equ_inv s'.
Proof.
  unfold rf_plus_af_equ_inv in *.
  intros i i0 c s s' H H0 i1 j H1 H2.
  inv_step.

  (* UncondReply *)
  unfold uncond_reply in *.
  autorewrite with fsmrw rfrw afrw in *. auto.

  (* ReceiveRequest *)
  unfold process_messages, acquire_lock in *.
  destruct (await_from (s i)).
  destruct (id_dec i i1).
  subst i1.
  smart_rewrite. 
  
  apply H.
  find_rewrite; congruence;
    (* autorewrite with fsmnerw in *. *)
    smart_rewrite.
  assumption.
  smart_rewrite. 
  apply H.
  autorewrite with fsmnerw in *.
  assumption.
  assumption.
  assumption.

  destruct (hd_error (mbox (s i))).
  destruct m.
  smart_rewrite. 
  auto.
  congruence.
  congruence.
Qed.  

Lemma ineq_transitive:
  forall {M:Type} (x m n : M),
    x = m -> m <> n -> x <> n. Proof. congruence. Qed.

Lemma rf_plus_af_equ_recv_reply_step:
  forall i i0 c s s',
    rf_plus_af_equ_inv s
    -> step s (ReceiveReply i i0 c) s'
    -> rf_plus_af_equ_inv s'.
Proof.
  unfold rf_plus_af_equ_inv in *.
  intros i i0 c s s' H H0 i1 j H1 H2.
  inv_step.
  autounfold with gendefs in *.
  
  destruct (await_from (s i)).
  (* await_from = [] *)
  unfold acquire_lock in *.  
  
  destruct (id_dec i1 i).  
  (* ----------- *)
  (* await_from=[], i1 = i case *)
  (* ----------- *)
  subst i1.

  smart_rewrite. clear H1.
  find_eapply_lem_hyp (@ineq_transitive FsmState); eauto; congruence.
  
  (* ------------ *)
  (* i1 <> i case *)
  (* ------------ *)
  autorewrite with rfrw afrw fsmnerw in *. eauto. auto.
  (* await_from =/= [] *)
  destruct (hd_error (mbox (s i))) eqn: Hm.
  destruct m eqn:H'.
  (* m = Request *)  
  find_inversion.
  
  (* m = Reply *)
  find_inversion.

  destruct (id_dec i i1).
  subst i1.
  smart_rewrite. 
  apply in_or_app. 

  find_apply_lem_hyp H.
  find_apply_lem_hyp in_app_or.
  break_or_hyp.
  
  left.
  unfold receive_reply.
  rewrite Hm.
  break_if.
  break_if.
  (* autorewrite with rfrw. *)
  smart_rewrite.
  simpl. right.  assumption.
  (* autorewrite with rfrw. *)
  smart_rewrite.
  simpl. right. assumption.
  smart_rewrite. (* autorewrite with rfrw. *)
  assumption. 

  unfold receive_reply.
  rewrite Hm.
  break_if.
  break_if.
  smart_rewrite.
  simpl.
  find_apply_lem_hyp remove_first_eq_or.
  break_or_hyp.
  find_rewrite. simpl in *. contradiction.
  find_rewrite. simpl in *. break_or_hyp.
  left. left. reflexivity.
  contradiction.

  smart_rewrite.
  simpl in *.

  destruct (id_dec i0 j).
  left. left. assumption.
  right.
  apply in_remove_first_other_in with (i:=j) in Heql0.
  simpl in *.
  break_or_hyp.
  left. reflexivity. 
  right. assumption. auto. assumption.

  smart_rewrite. right. assumption.
  find_rewrite. congruence.

  unfold receive_reply in *.
  repeat find_rewrite.
  repeat break_match;
    smart_rewrite; autorewrite with fsmnerw rfnerw afnerw in *; smart_rewrite; auto.
  auto.
Qed.
  
Lemma rf_plus_af_equ_unlock_step:
  forall i s s',
    rf_plus_af_equ_inv s
    -> step s (Unlock i) s'
    -> rf_plus_af_equ_inv s'.
Proof.
  unfold rf_plus_af_equ_inv in *.
  intros i s s' H H0 i0 j H1 H2.
  inv_step.
  unfold unlock.
  smart_rewrite.

  destruct (id_dec i0 i); subst.  
  autorewrite with fsmrw in *. congruence.
  autorewrite with fsmnerw rfnerw rfrw in *.
  auto. auto. auto.
Qed.

Hint Resolve rf_plus_af_equ_inv_lock_step
     rf_plus_af_equ_recv_request_step
     rf_plus_af_equ_recv_reply_step
     rf_plus_af_equ_unlock_step.

Theorem invariant_rf_plus_af_equ_inv:
  invariant rf_plus_af_equ_inv.
Proof.
  unfold invariant.
  split.
  unfold invariant_init; unfold rf_plus_af_equ_inv; simpl in *; intuition.
  unfold invariant_step; destruct op; eauto.
Qed.

Definition in_initial_deferred_nul (st: GState) :=
  forall i,
    fsm_state (st i) = Initial 
    -> (deferred (st i) = [] /\ received_from (st i) = []).

Theorem invariant_in_initial_deferred_nul:
  invariant in_initial_deferred_nul.
Proof.
  unfold invariant.
  split.
  unfold invariant_init, in_initial_deferred_nul.
  simpl. auto. 

  unfold invariant_step, in_initial_deferred_nul.
  intros.
  inv_step.
  smart_rewrite.
  autounfold with gendefs in *.
  smart_rewrite.
  two_branches.
  smart_rewrite.
  congruence.
  smart_rewrite.
  eauto. auto.

  autounfold with gendefs in *.
  smart_rewrite.
  eauto.

  autounfold with gendefs in *.
  repeat break_match.
  autounfold with gendefs in *.  
  smart_rewrite.
  two_branches.
  congruence.
  eauto.

  auto.
  find_inversion.
  smart_rewrite.
  eauto.

  find_inversion.
  smart_rewrite.
  eauto.
  find_inversion.
  smart_rewrite.
  two_branches.
  congruence.
  smart_rewrite.
  eauto.
  auto.

  find_inversion.
  smart_rewrite.
  two_branches.
  congruence.
  smart_rewrite. eauto.
  auto.

  autounfold with gendefs in *.
  repeat break_match.
  find_inversion.
  find_inversion.
  find_inversion.
  find_inversion.
  find_inversion.
  congruence.

  autounfold with gendefs in *.
  repeat break_match.
  autounfold with gendefs in *.
  smart_rewrite.
  two_branches.
  congruence.
  eauto.
  auto.
  smart_rewrite.
  find_inversion.

  smart_rewrite.

  find_inversion.

  smart_rewrite.

  find_inversion.

  smart_rewrite.
  find_inversion.
  find_inversion.

  smart_rewrite.
  autounfold with gendefs in *.
  repeat break_match.
  find_inversion.
  find_inversion.
  two_branches.
  congruence.
  smart_rewrite.
  two_branches.
  congruence.
  smart_rewrite.
  auto.
  auto.
  auto.
  find_inversion.
  smart_rewrite.
  two_branches.
  congruence.
  smart_rewrite.
  eauto.
  auto.
  find_inversion.
  smart_rewrite.
  eauto.
  
  congruence.
  congruence.
  
  autounfold with gendefs in *.
  smart_rewrite.
  two_branches.
  smart_rewrite.
  auto.
  smart_rewrite.
  autorewrite with fsmnerw in *.
  smart_rewrite.
  autorewrite with dfnerw.
  smart_rewrite.
  eauto.
  all:auto.
Qed.

Definition in_deferred_clock (st: GState) :=
  forall i j i_clk j_clk,
    In (i, i_clk, j_clk) (deferred (st j))
    -> j_clk = clock (st j).

Theorem invariant_in_deferred_clock:
  invariant2 in_deferred_clock in_initial_deferred_nul.
Proof.
  unfold invariant2, in_deferred_clock.
  split.
  unfold invariant_init.
  simpl.
  contradiction.

  unfold invariant_step2.

  intros.
  inv_step.

  smart_rewrite.
  autounfold with gendefs.
  smart_rewrite.
  two_branches.
  unfold in_initial_deferred_nul in *.
  find_apply_hyp_hyp.
  break_and.
  find_rewrite.
  simpl in *.
  contradiction.

  smart_rewrite.
  eauto.
  auto.

  autounfold with gendefs in *.
  smart_rewrite.
  eauto.
  
  autounfold with gendefs in *.
  repeat break_match.
  autounfold with gendefs in *.  
  smart_rewrite.
  eauto.

  find_inversion.
  smart_rewrite.
  eauto.

  find_inversion.
  smart_rewrite.
  eauto.

  find_inversion.
  smart_rewrite.
  two_branches.
  smart_rewrite.
  simpl in *.  break_or_hyp. find_inversion.
  reflexivity.
  eauto.

  autorewrite with dfnerw in *.
  smart_rewrite.
  eauto.
  auto.

  find_inversion.
  smart_rewrite.
  two_branches.
  smart_rewrite.
  simpl in *.
  break_or_hyp.
  find_inversion.
  reflexivity.
  eauto.
  
  autorewrite with dfnerw in *.
  smart_rewrite.
  eauto.
  auto.
  find_inversion.
  congruence.

  autounfold with gendefs in *.
  repeat break_match.
  autounfold with gendefs in *.  
  smart_rewrite.
  eauto.

  find_inversion.
  find_inversion.
  find_inversion.
  find_inversion.
  find_inversion.


  smart_rewrite.
  eauto.

  congruence.
  unfold unlock in *.
  smart_rewrite.
  two_branches.
  smart_rewrite.
  simpl in *. contradiction.
  autorewrite with dfnerw in *.
  smart_rewrite.
  eauto.
  auto.
Qed.

  
Definition in_mbox_sent (st: GState) :=
  forall j j_clk i i_clk, 
    i <> j
    -> In (Reply j j_clk i_clk) (mbox (st i)) 
    -> In (i,i_clk,j_clk) (reply_sent (st j)).

Lemma inv_in_mbox_sent_lock_step:
  forall i s s',
    in_mbox_sent s
    -> step s (Lock i) s'
    -> in_mbox_sent s'.
Proof.
  intros.
  unfold in_mbox_sent in *.
  inversion H0; subst; clear H0.

  intros.
  unfold send_all_requests in *.
  smart_rewrite.

  destruct (id_dec i0 i);
    subst;
    apply H;
    try assumption;
    [erewrite <- mbox_send_next_request_self; eassumption | 
     eapply in_mbox_reply_send_next_request; eassumption].
Qed.

Lemma in_reply_mbox_remove_msg:
  forall msg i s j,
    In msg (mbox (remove_msg i s j)) ->
    In msg (mbox (s j)).
Proof.
  intros.
  unfold remove_msg, update in *.
  destruct (id_dec i j); subst; simpl in *; auto using in_tail_implies.
Qed.

Lemma inv_in_mbox_sent_receive_request_step:
   forall i i0 c s s',
     in_mbox_sent s
     -> step s (ReceiveRequest i i0 c) s'
     -> in_mbox_sent s'.
Proof.
  intros.
  unfold in_mbox_sent in *.
  intros.
  inv_step.

  (* uncond_reply*)
  autounfold with gendefs in *.
  smart_rewrite.
  destruct (id_dec i j); subst.
  smart_rewrite.
  destruct (id_dec i0 i1); subst.
  autorewrite with mboxnerw in *.
  find_apply_lem_hyp in_app_or.
  break_or_hyp.
  find_apply_lem_hyp in_reply_mbox_remove_msg.  
  simpl. right. auto.
  simpl in *. break_or_hyp. find_inversion.
  left. reflexivity.
  contradiction.
  auto.
  autorewrite with mboxnerw in *.
  find_apply_lem_hyp in_reply_mbox_remove_msg.  
  simpl. right. auto. auto.
  autorewrite with rsnerw rsrw in *.
  destruct (id_dec i0 i1); subst.
  unfold send_reply in *.
  break_match ; subst.
  find_apply_lem_hyp in_reply_mbox_remove_msg.
  eauto.
  unfold send,update in *.
  break_match; subst.
  simpl in *.
  find_apply_lem_hyp in_app_or. break_or_hyp.
  find_apply_lem_hyp in_reply_mbox_remove_msg.
  eauto.
  simpl in *. break_or_hyp.
  find_inversion.
  congruence.
  contradiction.
  find_apply_lem_hyp in_reply_mbox_remove_msg.
  eauto.
  autorewrite with mboxnerw in *.
  find_apply_lem_hyp in_reply_mbox_remove_msg.
  eauto.
  auto. 
  auto.
  auto.

  unfold process_messages, acquire_lock in *.
  repeat break_match.
  smart_rewrite.
  eauto.
  find_inversion.

  unfold receive_request in *.
  repeat break_match.
  smart_rewrite.
  destruct (id_dec i i1); subst.
  find_inversion.
  destruct (id_dec i0 i1); subst.
  smart_rewrite.
  find_apply_lem_hyp (@in_tail_implies Msg).
  autorewrite with rsnerw rsrw.
  auto.
  auto.
  auto.
  auto.
  autorewrite with mboxnerw in *.
  find_apply_lem_hyp in_reply_mbox_remove_msg.
  autorewrite with rsnerw rsrw.
  auto.
  auto.
  auto.
  auto.
  find_inversion.
  destruct (id_dec i0 i1); subst.
  autorewrite with mboxnerw in *.
  find_apply_lem_hyp in_app_or. break_or_hyp.
  find_apply_lem_hyp in_reply_mbox_remove_msg.
  destruct (id_dec i j); subst.
  smart_rewrite.
  simpl. right. auto.
  autorewrite with rsnerw rsrw.
  auto.
  auto.
  auto.
  simpl in *. break_or_hyp.
  find_inversion.
  smart_rewrite.
  simpl. left. reflexivity.
  contradiction. auto.
  destruct (id_dec i j); subst.
  smart_rewrite.  
  autorewrite with mboxnerw in *; simpl.
  find_apply_lem_hyp in_reply_mbox_remove_msg. right; auto.
  auto.
  autorewrite with rsnerw rsrw.
  autorewrite with mboxnerw in *; simpl.
  find_apply_lem_hyp in_reply_mbox_remove_msg. auto.
  
  auto.
  auto.
  auto.

  find_inversion.
  smart_rewrite.
  destruct (id_dec i0 i1); destruct (id_dec i j); subst.
  smart_rewrite.
  autorewrite with mboxnerw in *; simpl.
  find_apply_lem_hyp in_app_or.
  break_or_hyp.
  find_apply_lem_hyp in_reply_mbox_remove_msg. right. auto.
  simpl in *. break_or_hyp.
  find_inversion.

  left. reflexivity.
  contradiction.
  auto.
  autorewrite with rsnerw rsrw.
  unfold send_reply,update in *. break_match; subst.
  find_apply_lem_hyp in_reply_mbox_remove_msg.
  auto.
  unfold send, update in *. break_match; subst. simpl in *.
  find_apply_lem_hyp in_app_or.
  break_or_hyp.
  find_apply_lem_hyp in_reply_mbox_remove_msg.
  auto.
  simpl in *. break_or_hyp. find_inversion. congruence.
  contradiction.
  find_apply_lem_hyp in_reply_mbox_remove_msg.
  auto.
  auto.
  auto.
  smart_rewrite.
  autorewrite with mboxnerw in *.
  find_apply_lem_hyp in_reply_mbox_remove_msg.
  simpl. right. auto.
  auto.
  autorewrite with mboxnerw rsnerw rsrw in *.
  find_apply_lem_hyp in_reply_mbox_remove_msg.
  auto.
  auto.
  auto.
  auto.
  find_inversion.
  smart_rewrite.
  find_apply_lem_hyp in_reply_mbox_remove_msg.
  auto.
  smart_rewrite.
  find_apply_lem_hyp in_reply_mbox_remove_msg.
  auto.
  find_inversion.
  congruence.
  find_inversion.
  congruence.
Qed.

Lemma inv_in_mbox_sent_receive_reply_step:
  forall i i0 c s s',
    in_mbox_sent s
    -> step s (ReceiveReply i i0 c) s'
    -> in_mbox_sent s'.
Proof.
  unfold in_mbox_sent.
  intros i i0 c s s' H H0 j j_clk i1 i_clk Hneq Hin.
  inv_step.
  unfold process_messages, acquire_lock, receive_request, receive_reply in *.

  destruct_id_dec.
  autorewrite with mboxrw rsrw in *.
  auto. congruence. congruence. congruence. congruence.
  find_inversion.

  unfold acquire_lock in *.
  autorewrite with rsrw mboxrw in *.
  
  destruct (id_dec i i1). 
  subst i1.

  autorewrite with mboxrw in *.
  apply H. assumption. in_tail_implies.
  assumption. auto.
  Ltac in_mbox_remove := find_apply_lem_hyp in_reply_mbox_remove_msg.
  in_mbox_remove.
  auto.

  autorewrite with mboxrw in *.
  in_mbox_remove.
  find_inversion.  

  destruct (id_dec i i1).
  subst i1.
  autorewrite with mboxrw rsrw in *.
  apply H. assumption. 
  auto.
  smart_rewrite. auto.
  in_mbox_remove.
  smart_rewrite.
  auto.
  congruence.
Qed.

Lemma inv_in_mbox_sent_unlock_step:
  forall i s s',
    in_mbox_sent s /\ in_deferred_clock s
    -> step s (Unlock i) s'
    -> in_mbox_sent s'.
Proof.
  unfold in_mbox_sent.
  intros.
  inv_step.

  unfold unlock in *.
  smart_rewrite.
  two_branches.

  apply in_or_app.
  find_apply_lem_hyp mbox_send_next_reply_in_df.
  break_or_hyp.
  right. eauto.
  break_and.
  break_exists.
  break_and. subst.
  eauto.
  break_exists.
  break_and.
  unfold in_deferred_clock in *.
  assert (H0' := H0).
  find_apply_hyp_hyp.
  subst.
  left. assumption.
  
  auto.

  smart_rewrite.
  neq_branch.

  destruct (id_dec i0 i). subst i0.
  rewrite mbox_send_next_reply_self in *.
  break_and.
  eauto.
  apply mbox_send_next_reply_diff in H2.
  break_and.
  eauto. auto.
  auto. auto.
Qed.

Hint Resolve
     inv_in_mbox_sent_lock_step
     inv_in_mbox_sent_receive_request_step
     inv_in_mbox_sent_receive_reply_step
     inv_in_mbox_sent_unlock_step.

Theorem invariant_in_mbox_sent:
  invariant3 in_mbox_sent in_deferred_clock in_initial_deferred_nul.
Proof.
  unfold invariant.
  split.
  unfold invariant_init. unfold in_mbox_sent. simpl in *. intuition.

  unfold invariant_step3.
  destruct op ; eauto.
Qed.

Definition in_received_from_impl (st:GState) :=
  forall i j j_clk i_clk, 
    i <> j
    -> In (j,j_clk,i_clk) (received_from (st i)) 
    -> i_clk = clock (st i).

Theorem invariant_in_received_from_impl:
  invariant2 in_received_from_impl in_initial_deferred_nul.
Proof.
  unfold invariant2, invariant_init, in_received_from_impl, invariant_step2.
  split.
  simpl. intuition.
  
  intros.

  inv_step.
  smart_rewrite.
  unfold send_all_requests.
  smart_rewrite.
  two_branches.
  unfold in_initial_deferred_nul in *.
  find_apply_hyp_hyp.
  break_and.
  find_rewrite.
  simpl in * ; contradiction.
  smart_rewrite.
  eauto.
  auto.

  autounfold with gendefs in *; smart_rewrite; eauto.
  autounfold with gendefs in *; repeat break_match; smart_rewrite; eauto.
  find_inversion.

  autounfold with gendefs in *; repeat break_match; smart_rewrite; eauto.
  find_inversion.
  autounfold with gendefs in *; repeat break_match; smart_rewrite; eauto.
  find_inversion.
  two_branches.
  smart_rewrite.
  simpl in *.
  break_or_hyp.
  find_inversion.
  reflexivity.
  eauto.
  smart_rewrite.
  eauto.
  auto.
  find_inversion.
  two_branches.
  simpl in *.
  break_or_hyp.

  find_inversion.
  reflexivity.

  eauto.
  smart_rewrite.
  eauto.

  auto.
  
  unfold unlock in *; smart_rewrite.
  two_branches; [simpl in *; contradiction | smart_rewrite | assumption].
  eauto.
Qed.

 
Definition locked_implies_two_things (st: GState) :=
  forall i,
    (fsm_state (st i) = Locked \/ fsm_state (st i) = Initial)
    -> await_from (st i) = [].

Theorem invariant_locked_implies_two_things:
  invariant locked_implies_two_things.
Proof. 
  unfold invariant.
  split.
  unfold invariant_init.
  unfold locked_implies_two_things.
  intros.
  simpl in H. inversion H.
  inversion H0. simpl. reflexivity.
  (*inv_step*)
  unfold invariant_step.
  intros.
  unfold locked_implies_two_things in *.
  intros.

  inv_step.

  destruct (id_dec i0 i); subst.
  unfold send_all_requests in *. 
  autorewrite with fsmrw in *. break_or_hyp.
  congruence. congruence.
  autorewrite with fsmnerw in *.
  rewrite af_send_all_requests_other. eauto. assumption. assumption.


  unfold uncond_reply in *.
  smart_rewrite.
  eauto.
  
  (* op = Lock *)
  destruct (id_dec i0 i); subst.
  unfold process_messages, acquire_lock, receive_reply, receive_request in *.
  autorewrite with fsmrw in *.
  break_match.
  autorewrite with afrw. assumption.
  repeat find_rewrite.
  break_if.
  autorewrite with fsmrw afrw in *.
  auto.
  
  (* op = ReceiveRequest *)
  break_if. break_if;
  (*inversion H0; subst.*)
  destruct (id_dec i0 i); subst;
  repeat autorewrite with fsmrw afrw in *;
  auto.
  
  autorewrite with fsmrw afrw in *.
  auto.

  unfold process_messages, acquire_lock, receive_reply, receive_request in *.
  autorewrite with fsmrw in *.
  break_match.
  autorewrite with fsmnerw afnerw afrw in *.
  eauto. auto.
  break_match.  break_match. break_if. autorewrite with fsmrw afrw in *. auto.
  find_inversion. break_if. break_if. autorewrite with fsmrw afrw in *. auto.
  autorewrite with fsmrw afrw in *. auto.
  autorewrite with fsmrw afrw in *. auto.
  find_inversion. congruence.
  
  (* op =ReceiveReply *)
  unfold process_messages, acquire_lock, receive_reply, receive_request in *.
  repeat find_rewrite. break_match.
  destruct (id_dec i0 i); subst;
  repeat autorewrite with fsmrw afrw in *.
  assumption. 
  autorewrite with fsmnerw afnerw afrw in *.
  eauto. auto.
  break_match.  break_match. autorewrite with fsmrw afrw in *. auto.
  destruct (id_dec i0 i); subst.
  autorewrite with fsmrw afrw in *. auto.
  autorewrite with fsmnerw afnerw fsmrw afrw in *. auto. auto. auto.
  autorewrite with fsmrw afrw in *.
  destruct (id_dec i0 i); subst.
  break_or_hyp; find_rewrite; congruence.
  autorewrite with fsmnerw afnerw fsmrw afrw in *. auto. auto. 
  autorewrite with fsmrw afrw in *.
  eauto.

  
  (* op = Unlock *)
  destruct (id_dec i0 i).
  subst i0.
  unfold unlock.
  autorewrite with fsmrw fsmnerw afrw afnerw.
  eauto.
  unfold unlock in *.
  autorewrite with fsmrw fsmnerw afrw afnerw in *.
  eauto.
  auto.
Qed.

Definition in_rf_in_rs (st:GState) :=
  forall i j j_clk i_clk, 
    i <> j
    -> In (j,j_clk,i_clk) (received_from (st i)) 
    -> In (i,i_clk,j_clk) (reply_sent (st j)).

Theorem invariant_in_rf_in_rs:
  invariant4 in_rf_in_rs in_mbox_sent in_deferred_clock in_initial_deferred_nul.
Proof. 
  unfold invariant4.
  split.
  unfold invariant_init. unfold in_rf_in_rs in *. intros.
  simpl in *. contradiction.

  unfold invariant_step4.
  intros.
  unfold in_rf_in_rs in *.
  intros.

  inv_step.
  autounfold with gendefs in *; smart_rewrite.
  auto.
  autounfold with gendefs in *; smart_rewrite.
  destruct (id_dec i0 j); subst.
  smart_rewrite.
  simpl. right. auto.
  autorewrite with rsnerw rsrw.
  auto. auto. auto.

  repeat autounfold with gendefs in *; smart_rewrite; repeat break_match; smart_rewrite; auto; subst.
  find_inversion.
  (* op = Request *)
  match goal with
    | [|- context[reply_sent(update_reply_sent _ ?X _ ?Y)]] =>
       destruct (id_dec X Y); subst;
       [ autorewrite with rsrw | autorewrite with rsnerw]
  end.
  simpl. right. auto.
  smart_rewrite. auto. auto. auto.
  find_inversion.  
  two_branches. simpl. right. auto.
  smart_rewrite. auto. assumption. assumption.

  (* op = Reply *)
  congruence.
  congruence.

  repeat autounfold with gendefs in *; smart_rewrite; repeat break_match; smart_rewrite; auto; subst.
  find_inversion.
  find_inversion.
  find_inversion.

  two_branches.
  simpl in *. break_or_hyp.
  tuple_inversion.
  break_if; subst.
  find_apply_lem_hyp (@hd_in_implies Msg).
  eauto.
  trivial.
  congruence.
  auto.
  find_apply_lem_hyp (@hd_in_implies Msg).
  repeat smart_rewrite; auto.
  trivial. assumption.

  two_branches.
  simpl in *. break_or_hyp.
  tuple_inversion.
  break_if; subst.
  find_apply_lem_hyp (@hd_in_implies Msg).
  eauto.
  trivial.
  
  find_inversion.
  find_inversion.
  auto using (@hd_in_implies Msg).
  find_inversion.
  auto.
  find_inversion.

  smart_rewrite.
  auto.
  auto.

  (* op = Unlock *)
  destruct (id_dec i0 i).
  subst i0.
  unfold unlock in *.
  autorewrite with rfrw in *.
  simpl in *. contradiction.

  autorewrite with rfnerw in *.
  destruct  (id_dec i0 j).
  subst i0.
  unfold unlock.
  autorewrite with rsrw.
  apply in_or_app.
  right.
  eauto.
  unfold unlock.
  smart_rewrite.
  eauto.
  smart_rewrite.
  neq_branch.
  smart_rewrite.
  neq_branch.
  auto. auto. auto. auto.
Qed. 


Lemma le_trans_not_lt:
  forall a b c,
    a <= b -> b <= c -> ~ (c < a).
Proof.
  intros. apply le_trans with (m:=b) (p:=c) in H. 
  apply le_not_lt. assumption. assumption.
Qed.

Lemma lt_vclock_equal:
  forall i j i_clk j_clk st,
    lt_vclock i (clock (st i)) j j_clk
    -> lt_vclock j (clock (st j)) i i_clk
    -> j_clk <= clock (st j)
    -> i_clk <= clock (st i)
    -> i = j.
Proof.
  intros.
  unfold lt_vclock in *.
  destruct (id_lt_dec i j).
  inversion H0; clear H0.
  inversion H; clear H.  
  apply lt_le_trans with (m:=j_clk) (p:=clock (st j)) in H0.
  apply lt_le_trans with (m:=i_clk) (p:=clock (st i)) in H3.
  apply lt_asym in H3. apply H3 in H0. contradiction.
  assumption. assumption.
  inversion H0; subst; clear H0.
  apply le_trans_not_lt with (b:=clock (st i)) (c:=clock (st j)) in H2. 
  apply H2 in H3. contradiction.
  assumption.
  inversion H3; clear H3.
  unfold id_lt in H4. apply lt_asym in H4. apply H4 in l. contradiction. 
  inversion H; subst; clear H.
  inversion H0; subst; clear H0.
  apply lt_le_trans with (m:=j_clk) (p:=clock (st j)) in H3.
  apply lt_le_trans with (m:=i_clk) (p:=clock (st i)) in H.
  apply lt_asym in H3. apply H3 in H. contradiction.
  assumption.
  assumption.
  inversion H; subst; clear H.
  apply le_trans_not_lt with (b:=(clock (st j))) (c:=(clock (st i))) in H1.
  apply H1 in H3. contradiction.
  assumption.
  inversion H0; subst; clear H0.
  inversion H3; subst; clear H3.
  apply le_trans_not_lt with (b:=(clock (st i))) (c:=(clock (st j))) in H2.
  apply H2 in H. contradiction.
  assumption.
  inversion H3; subst; clear H3.
  unfold id_lt in H4. apply n in H4. contradiction.
Qed.

Lemma in_get_id_implies_in:
  forall (l:list RequestSig) j, 
    In j (get_id l) ->
    (exists j_clk i_clk, In (j, j_clk, i_clk) l).
Proof.
  induction l.
  simpl. intuition.

  intros.
  simpl in H.
  unfold filt_fun in H.
  inversion H; clear H.
  destruct a. destruct p.
  subst j.
  exists c0, c.
  simpl. left. reflexivity.
  eapply IHl in H0.
  inversion H0. inversion H.
  exists x, x0.
  simpl. right. assumption.
Qed.

Definition in_deferred_impl (st: GState) :=
  forall (i j:ID) (i_clk j_clk:Clock), 
    i <> j
    -> In (j,j_clk,i_clk) (deferred (st i)) 
    -> j_clk <= deferred_clock (st i).

Theorem invariant_in_deferred_impl:
  invariant in_deferred_impl.
Proof.
  unfold invariant.
  split.
  unfold invariant_init.
  unfold in_deferred_impl.
  simpl.
  contradiction.

  unfold invariant_step.
  intros.
  unfold in_deferred_impl in *.
  intros.
  inv_step.

  autounfold with gendefs in *;
  smart_rewrite;
  eauto.
  autounfold with gendefs in *;
    smart_rewrite.
  destruct (id_dec i0 i); subst.
  smart_rewrite.
  rewrite NPeano.Nat.max_le_iff. left. eauto.
  autorewrite with dfclknerw dfclkrw.
  eauto. auto.

  autounfold with gendefs in *;
    repeat break_match; subst.
  autounfold with gendefs in *;
    repeat break_match; subst.
  smart_rewrite.
  eauto.
  find_inversion.
  smart_rewrite.
  two_branches.
  rewrite NPeano.Nat.max_le_iff. left. eauto.
  autorewrite with dfclknerw dfclkrw.
  eauto. auto.

  find_inversion.
  smart_rewrite.
  two_branches.
  rewrite NPeano.Nat.max_le_iff. left. eauto.
  autorewrite with dfclknerw dfclkrw.
  eauto. auto.

  smart_rewrite.
  two_branches.
  smart_rewrite.
  simpl in *. break_or_hyp.
  tuple_inversion.
  rewrite NPeano.Nat.max_le_iff. right. eauto.
  find_inversion.
  rewrite NPeano.Nat.max_le_iff. left. eauto.
  smart_rewrite.
  two_branches.
  smart_rewrite.
  contradiction.
  autorewrite with dfnerw dfrw in *.
  eauto.
  autorewrite with dfnerw dfrw in *.
  eauto.
  auto.
  auto.
  find_inversion.

  smart_rewrite.
  two_branches.


  smart_rewrite.
  simpl in *.
  break_or_hyp.
  find_inversion.

  rewrite Nat.max_le_iff. right. auto.
  rewrite Nat.max_le_iff. left. eauto.
  smart_rewrite.
  autorewrite with dfnerw dfrw in *.
  eauto.
  auto.
  auto.
  find_inversion.
  congruence.

  autounfold with gendefs in *;
    repeat break_match; subst.
  autounfold with gendefs in *;
    repeat break_match; subst.

  smart_rewrite. eauto.
  find_inversion.
  find_inversion.

  find_inversion.
  find_inversion.
  find_inversion.
  smart_rewrite.
  autounfold with gendefs in *;
    repeat break_match; subst.
  find_inversion.
  find_inversion.
  smart_rewrite.
  two_branches.
  rewrite Nat.max_le_iff. left. eauto.
  smart_rewrite.
  eauto.
  auto.
  find_inversion.

  two_branches.
  rewrite Nat.max_le_iff. left. eauto.
  smart_rewrite.
  eauto.
  auto.
  find_inversion.
  smart_rewrite. eauto.
  congruence.
  congruence.

  autounfold with gendefs in *;
    repeat break_match; subst.

  smart_rewrite.
  two_branches.
  smart_rewrite.
  simpl in *. contradiction.
  autorewrite with dfnerw dfrw in *.
  eauto.
  auto.
Qed.
  
Definition in_rs_lt_deferred (st: GState) :=
  forall j j_clk i i_clk, 
    i <> j
    -> In (i,i_clk,j_clk) (reply_sent (st j)) 
    -> i_clk <= deferred_clock (st j).

Theorem invariant_in_rs_lt_deferred:
  invariant2 in_rs_lt_deferred in_deferred_impl.
Proof.
  unfold invariant2.
  split.
  unfold invariant_init.
  unfold in_rs_lt_deferred.
  simpl.
  contradiction.

  unfold invariant_step2.
  intros.
  unfold in_rs_lt_deferred in *.
  intros.
  inv_step.
  unfold send_all_requests in *.
  autorewrite with rsrw dfclkrw in *.
  eauto.

  unfold uncond_reply in *.
  autorewrite with rsrw dfclkrw in *.
  destruct (id_dec i0 j); subst.
  autorewrite with rsrw dfclkrw in *.
  simpl in *. break_or_hyp.
  find_inversion.
  rewrite NPeano.Nat.max_le_iff. right. reflexivity.

  find_apply_hyp_hyp.
  rewrite NPeano.Nat.max_le_iff. left. assumption.

  autorewrite with rsnerw dfclknerw dfclkrw rsrw in *.
  eauto.
  auto.
  auto.
  auto.

  unfold process_messages in *.
  repeat break_match.
  unfold acquire_lock in *.
  autorewrite with rsrw dfclkrw in *.
  eauto.
  find_inversion.
  unfold receive_request in *.
  repeat break_match.

  destruct (id_dec i0 j); subst.
  autorewrite with rsrw dfclkrw in *.
  simpl in *. break_or_hyp.
  find_inversion.
  find_inversion.
  rewrite NPeano.Nat.max_le_iff. right. reflexivity.
  find_inversion.
  find_apply_hyp_hyp.
  rewrite NPeano.Nat.max_le_iff. left. assumption.

  autorewrite with rsnerw dfclknerw rsrw dfclkrw in *.

  eauto.
  auto.
  auto.
  auto.

  find_inversion.
  destruct (id_dec i0 j); subst.
  autorewrite with rsrw dfclkrw in *.
  simpl in *. break_or_hyp.
  find_inversion.
  rewrite NPeano.Nat.max_le_iff. right. auto.

  rewrite NPeano.Nat.max_le_iff. left. eauto.

  autorewrite with rsnerw dfclknerw rsrw dfclkrw in *.
  eauto.
  auto. auto. auto.
  find_inversion.
  destruct (id_dec i0 j); subst.  
  autorewrite with  rsrw dfclkrw in *.
  rewrite NPeano.Nat.max_le_iff. left. eauto.
  autorewrite with  rsrw dfclknerw dfclkrw in *.
  eauto.
  auto.
  autorewrite with rsrw dfclkrw in *.
  find_inversion.
  destruct (id_dec i0 j); subst.
  autorewrite with dfclkrw in *.
  rewrite NPeano.Nat.max_le_iff. left. eauto.
  autorewrite with dfclknerw dfclkrw in *.
  eauto.
  auto.
  find_inversion.
  eauto.
  find_inversion.
  congruence.
  unfold process_messages in *.

  repeat break_match.
  unfold acquire_lock in *.
  autorewrite with rsrw dfclkrw in *.
  eauto.

  find_inversion.
  find_inversion.

  unfold receive_reply in *.
  repeat break_match.
  find_inversion.
  find_inversion.
  destruct (id_dec i0 j); subst.
  autorewrite with rsrw dfclkrw in *.
  rewrite NPeano.Nat.max_le_iff. left. eauto.
  
  autorewrite with rsrw dfclknerw dfclkrw in *. eauto. auto.
  find_inversion.

  destruct (id_dec i0 j); subst.
  autorewrite with rsrw dfclkrw in *.
  
  rewrite NPeano.Nat.max_le_iff. left. eauto.
  
  autorewrite with rsrw dfclknerw dfclkrw in *.
  eauto.
  auto.
  find_inversion.
  autorewrite with rsrw dfclkrw in *. eauto.
  congruence.
  congruence.
  unfold unlock in *.
  destruct (id_dec i0 j); subst.
  autorewrite with rsrw dfclkrw in *.
  
  find_apply_lem_hyp in_app_or. break_or_hyp.
  unfold in_deferred_impl in *.
  eauto.
  eauto.

  autorewrite with rsrw dfclkrw rsrw rsnerw in *.
  eauto.
  auto.
  auto.
Qed.
  

Definition permitted (st: GState) :=
  forall j j_clk i i_clk, 
    i <> j
    -> In (i,i_clk,j_clk) (reply_sent (st j)) 
    -> fsm_state (st j) = Initial \/ lt_vclock i i_clk j (clock (st j)).

Theorem invariant_permitted:
  invariant3 permitted in_rs_lt_deferred in_deferred_impl.
Proof.
  unfold invariant3.
  split.
  unfold invariant_init.
  unfold permitted.
  intros.
  simpl in *.
  contradiction.

  unfold invariant_step3.
  unfold permitted.
  intros.
  inv_step.

  unfold send_all_requests in *.
  autorewrite with rsrw fsmrw in *.
  destruct (id_dec j i0) ; subst.

  autorewrite with fsmrw clock.
  unfold in_rs_lt_deferred in *.
  find_apply_hyp_hyp.
  right.
  unfold lt_vclock. left.
  simpl. apply le_gt_S. auto.
  autorewrite with fsmnerw fsmrw clock clockne.
  eauto. auto.
  auto.

  unfold uncond_reply in *.
  destruct (id_dec i0 j); subst.
  autorewrite with fsmrw rsrw in *.
  simpl in *. break_or_hyp.
  find_inversion.
  left. auto.
  eauto.
  autorewrite with rsrw rsnerw fsmnerw fsmrw clock in *. eauto.
  auto. auto.
  
  unfold process_messages in *.
  repeat break_match.
  unfold acquire_lock in *.
  destruct (id_dec i0 j) ; subst.
  autorewrite with fsmrw rsrw clock in *.
  find_apply_hyp_hyp.
  break_or_hyp. congruence.
  right. assumption.
  autorewrite with fsmnerw rsnerw rsrw clock in *.
  eauto. auto.


  find_inversion.
  unfold receive_request in *.
  repeat break_match.
  destruct (id_dec i0 j) ; subst.
  autorewrite with rsrw in *.
  find_inversion.
  simpl in *.
  break_or_hyp. find_inversion.
  autorewrite with fsmrw clock. right.
  unfold lt_vclock. left. assumption.

  autorewrite with fsmrw clock.
  eauto.
  find_inversion.
  autorewrite with rsrw fsmrw rsnerw fsmnerw clock in *.
  eauto.
  auto.
  auto.
  find_inversion.

  autorewrite with rsrw fsmrw clock in *.
  destruct (id_dec i0 j); subst.
  autorewrite with rsrw in *.
  simpl in *. break_or_hyp.
  find_inversion.

  right.
  unfold lt_vclock.
  right.
  split. reflexivity.
  auto.
  eauto.

  autorewrite with rsnerw rsrw clock in *.
  eauto.
  auto.
  auto.
  find_inversion.
  autorewrite with rsrw fsmrw clock in *.
  eauto. 

  find_inversion.
  autorewrite with rsrw fsmrw clock in *.
  eauto.
  find_inversion.
  congruence.
  find_inversion.
  congruence.
  
  unfold process_messages in *.
  repeat break_match.
  unfold acquire_lock in *.
  destruct (id_dec i0 j); subst.

  autorewrite with rsrw fsmrw clock in *.

  find_apply_hyp_hyp.
  break_or_hyp. congruence.
  right. assumption.
  autorewrite with fsmnerw rsnerw rsrw fsmrw clock in *.
  eauto. auto.


  find_inversion.
  find_inversion.


  unfold receive_reply in *.
  repeat break_match.
  find_inversion.
  find_inversion.
  destruct (id_dec i0 j); subst.

  autorewrite with rsrw fsmrw clock in *.
  find_apply_hyp_hyp. break_or_hyp.
  congruence.

  right. assumption.
  autorewrite with rsnerw fsmnerw rsrw fsmrw clock in *.
  eauto.


  auto.
  autorewrite with rsrw fsmrw clock in *.
  eauto.
  autorewrite with rsrw fsmrw clock in *.
  eauto.
  autorewrite with rsrw fsmrw clock in *.
  eauto.
  congruence.

  unfold unlock in *.
  destruct (id_dec i0 j); subst.
  autorewrite with rsrw fsmrw clock in *.
  left. reflexivity.
  autorewrite with rsnerw fsmnerw rsrw fsmrw clock in *.
  eauto.
  auto.
  auto.
  auto.
Qed.
  
Theorem ra_mutex_safety:
  forall st t i j, 
    Clients <> []
    -> i <> j
    -> step_star Ginit_state t st     
    -> fsm_state (st i) = Locked
    -> ~ fsm_state (st j) = Locked.
Proof.
  intros st t i j H0 Hne Hst H1 H2.
  assert (Hrf := invariant_rf_plus_af_equ_inv).  
  assert (Hli := invariant_locked_implies_two_things). 
  assert (Hinr := invariant_in_received_from_impl). 
  assert (His := invariant_permitted).
  assert (Hrr := invariant_in_rs_lt_deferred).
  assert (Hrs := invariant_in_rf_in_rs).

  apply step_star_inv' with (t:=t) (s:=st) in Hli.
  apply step_star_inv2 with (t:=t) (s:=st) in Hinr.  
  apply step_star_inv' with (t:=t) (s:=st) in Hrf.
  
  apply step_star_inv3 with (t:=t) (s:=st) in His.
  apply step_star_inv4 with (t:=t) (s:=st) in Hrs.
  apply step_star_inv2 with (t:=t) (s:=st) in Hrr.
  
  2: apply invariant_in_deferred_impl.
  3: apply invariant_in_initial_deferred_nul.
  3: apply invariant_in_deferred_clock.  
  3: apply invariant_in_mbox_sent.
  4: apply invariant_in_deferred_impl.
  4: apply invariant_in_rs_lt_deferred.
  6: apply invariant_in_initial_deferred_nul.
  2-7: assumption.
  
  assert (H6: fsm_state (st i) <> Initial). find_rewrite. discriminate.  
  assert (H7: fsm_state (st j) <> Initial). rewrite H2. discriminate.
  
  unfold locked_implies_two_things in Hli.    
  unfold in_received_from_impl in Hinr.
  unfold in_rf_in_rs in Hrr.
  unfold rf_plus_af_equ_inv in *.
  unfold permitted in *.
  assert (In j (known_to i)) by auto using in_id_known_to.
  assert (In i (known_to j)) by auto using in_id_known_to.
  
  eapply Hrf in H6.
  eapply Hrf in H7.
  assert (await_from (st j) = []) by auto using Hli.
  assert (await_from (st i) = []) by auto using Hli.
  find_rewrite.
  find_rewrite.
  rewrite app_nil_r in *.

  unfold in_rs_lt_deferred in *.
  clear H4 H5.
  unfold in_rf_in_rs in *.
  find_apply_lem_hyp in_get_id_implies_in.
  find_apply_lem_hyp in_get_id_implies_in.
  break_exists.

  assert(H5' := H5).
  assert(H4' := H4).
  eapply Hinr in H5 ; subst.
  eapply Hinr in H4 ; subst.
  eapply Hrs, His in H5'.
  eapply Hrs, His in H4'.
  instantiate (1:=j) in H4'.
  instantiate (1:=i) in H5'.
  
  break_or_hyp.
  congruence.
  break_or_hyp. congruence.
  assert (i = j).
  eapply lt_vclock_equal.
  eassumption.
  eassumption.
  reflexivity.
  reflexivity.
  congruence.
  all: auto.
Qed.