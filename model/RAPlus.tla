(* =================================== *)
(* RA+ protocol model                  *)
(* =================================== *)
(* Author: Evgeny.Shishkin@infotecs.ru *)

-------------------------------- MODULE RAPlus --------------------------------
EXTENDS Naturals, Sequences, TLC, FiniteSets

\* A set of process IDs
CONSTANTS p1, p2, p3   \* WARNING: Change IdLessThan accordingly
CONSTANT Process

\* =====================================================================================
\* FSM states
\* =====================================================================================
\* WaitStartProtocol: process not yet joined a cluster group
\* JoinGroup: process joined the group but not yet set clientups to group members
\* Initial: process sent client ups to group members and ready to start competing for a lock
\* ProcessMessages: process is trying to acquire a lock
\* Locked: process gained a lock
\* Stopped: process crashed or stopped normally

CONSTANTS WaitStartProtocol, JoinGroup, GetMembers,
	  Initial, ProcessMessages, Locked, Stopped
FsmStates == {WaitStartProtocol, JoinGroup, GetMembers,
	  Initial, ProcessMessages, Locked, Stopped}

\* Message types
CONSTANTS Request, Reply, ClientDown, ClientUp

\* Maximum number of times client allowed to ask a lock
\* This is needed to make model state space bounded
CONSTANTS MaxLocksPerClient

\* Maximum number of crashes allowed to happen 
CONSTANTS MaxCrashes

RequestMessage == [mtype: { Request }, src: Process, clock: Nat]
ReplyMessage == [mtype: { Reply }, src: Process]
ClientDownMessage == [mtype: { ClientDown }, src: Process]
ClientUpMessage == [mtype: { ClientUp }, src: Process]
Messages == Seq(RequestMessage \cup ReplyMessage \cup ClientDownMessage \cup ClientUpMessage)

\* Process variables
\* A state of a system is a state of each process of that system
VARIABLE await_from      \* Process -> SUBSET Process
VARIABLE deferred        \* Process -> [Process -> BOOLEAN]
VARIABLE clock           \* Process -> Nat
VARIABLE deferred_clock  \* Process -> Nat
\* Local view of the process about cluster group members
VARIABLE clients         \* Process -> SUBSET Process
VARIABLE fsm_state       \* Process -> FsmStates
VARIABLE mailbox         \* Process -> Messages

\* System variables

\* Global view of the cluster members
VARIABLE members         \* SUBSET Process
VARIABLE lock_count      \* Process -> Nat
VARIABLE netmon          \* network monitor record
VARIABLE monitor	 \* Process -> [Process -> BOOLEAN]


ASSUME Cardinality(Process) >= 2

vars ==
  << await_from, deferred, clock, deferred_clock, mailbox,
     fsm_state, members, clients, lock_count, netmon, monitor >>

noLockCount ==
  << await_from, deferred, clock, deferred_clock, mailbox,
    fsm_state, members, clients, netmon, monitor >>

Init ==
  /\ await_from = [i \in Process |-> {}]
  /\ deferred = [i \in Process |-> [j \in Process |-> FALSE]]
  /\ clock = [i \in Process |-> 0]
  /\ deferred_clock = [i \in Process |-> 0]
  /\ clients = [i \in Process |-> {}]
  /\ mailbox = [i \in Process |-> << >>]
  /\ fsm_state = [i \in Process |-> WaitStartProtocol]
  /\ members = { } \* Process
  /\ lock_count = [i \in Process |-> 0]
  /\ netmon = { }
  /\ monitor = [i \in Process |-> [j \in Process |-> FALSE]]

TypeInvariant ==
  /\ await_from \in [Process -> SUBSET Process]
  /\ members \in SUBSET Process
  /\ deferred \in [Process -> [Process -> BOOLEAN]]
  /\ clock \in [Process -> Nat]
  /\ deferred_clock \in [Process -> Nat]
  /\ mailbox \in [Process ->  Messages]
  /\ fsm_state \in [Process -> FsmStates]
  /\ clients \in [Process -> SUBSET Process]
  /\ lock_count \in [Process -> Nat]
  /\ netmon \in SUBSET (Process \X ClientDownMessage)
  /\ monitor \in [Process -> [Process -> BOOLEAN]]

Max(A,B) ==
  IF A >= B THEN A ELSE B

\* To allow multiple sends in one step we add Q argument:
\* actual queue on which operation has to be performed.
\* Now we can chain multiple queue operations like this:
\* Discard(SendBye(queue, i, j), i), i, j)
SendMessage(Q, dst, msg) ==     
  [Q EXCEPT ![dst] = Append(@, msg)]

\* Send a message to multiple processes in one step 
Broadcast(MB, src, group, msg) ==
  [i \in Process |-> IF i \in group THEN Append(MB[i], msg) ELSE MB[i]]

Discard(Q, dst) ==
  [Q EXCEPT ![dst] = Tail(@)]
CleanupMsgQueue(Q, dst) ==
  [Q EXCEPT ![dst] = << >>]

SendReply(MB, src, dst) ==
  SendMessage(MB, dst, [mtype |-> Reply, src |-> src])

\* Join the process group using group membership facility
WS_JoinGroup(i) ==
    /\ members' = members \cup {i} \* join():  mutex process enters the group
    /\ fsm_state' = [fsm_state EXCEPT ![i] = JoinGroup]
    /\ UNCHANGED << await_from, deferred, deferred_clock, clock, clients, lock_count,
       netmon, mailbox, monitor >>

\* Inform every member of the process group about our presence
JoinGroup_GetMembers(i) ==
 \* get_members(): mutex process asks group manager about current
 \* group members
  LET Others == members \ {i}  
  IN
    /\ fsm_state' = [fsm_state EXCEPT ![i] = GetMembers]
    /\ clients' = [clients EXCEPT ![i] = Others]
    /\ UNCHANGED << await_from, deferred, deferred_clock, clock, lock_count,
       netmon, members, mailbox, monitor >>

\* Inform every member of the process group about our presence
GetMembers_StartProtocol(i) ==
    /\ mailbox' = Broadcast(mailbox, i, clients[i], [mtype |-> ClientUp, src |-> i])
    /\ monitor' = [monitor EXCEPT ![i] =
        [j \in Process |-> IF (j \in clients[i]) THEN TRUE ELSE monitor[i][j]]]
    /\ fsm_state' = [fsm_state EXCEPT ![i] = Initial]
    /\ UNCHANGED << await_from, deferred, deferred_clock, clock, lock_count,
       netmon, members, clients >>

\* ============================================================================
\* Acquire action in Initial fsm state
\* ============================================================================
Initial_Acquire1(i) ==
   /\ Cardinality(clients[i]) > 0
   /\ lock_count[i] < MaxLocksPerClient
   /\ LET clk == 1 + deferred_clock[i] IN   \* very important!
     /\ mailbox' = Broadcast(mailbox, i, clients[i],
              [mtype |-> Request, src |-> i, clock |-> clk])
     /\ clock' = [clock EXCEPT ![i] = clk]
     /\ fsm_state' = [fsm_state EXCEPT ![i] = ProcessMessages]
     /\ await_from' = [await_from EXCEPT ![i] = clients[i] ]
     /\ UNCHANGED << deferred, deferred_clock, lock_count, netmon, members, clients,
         monitor >>

\* ============================================================================
\* Acquire action in Initial fsm state
\* ============================================================================
Initial_Acquire2(i) ==
   /\ Cardinality(clients[i]) = 0
   /\ mailbox[i] = << >>
   /\ lock_count[i] < MaxLocksPerClient
   /\ fsm_state' = [fsm_state EXCEPT ![i] = Locked]
   /\ UNCHANGED << deferred, deferred_clock, lock_count, netmon, members, clients,
        mailbox, await_from, clock, monitor >>

\* ============================================================================
\* Request message arrived in Initial fsm state, so send Reply message
\* unconditionally "Request_Critical_Section == FALSE"
\* ============================================================================
Initial_ReceiveRequest(i) ==
   /\ mailbox[i] /= << >>
   /\ LET msg == Head(mailbox[i])
     IN /\ msg.mtype = Request
        /\ LET maxseqnum == Max(msg.clock, deferred_clock[i])
          IN /\ mailbox' = SendReply(Discard(mailbox, i), i, msg.src)
	     /\ deferred_clock' = [deferred_clock EXCEPT ![i] = maxseqnum]
	     /\ UNCHANGED << deferred, await_from, fsm_state, lock_count,
	          clock, netmon, members, clients, monitor >>

\* ============================================================================
\* ReceiveReply action in ProcessMessages fsm state
\* ============================================================================
PM_ReceiveReply1(i) ==
   /\ Cardinality(await_from[i]) = 1
   /\ mailbox[i] /= << >>
   /\ LET msg == Head(mailbox[i])
     IN /\ msg.mtype = Reply
        /\ await_from[i] = {msg.src}
	/\ await_from' = [await_from EXCEPT ![i] = { }]
	/\ fsm_state' = [fsm_state EXCEPT ![i] = Locked]
        /\ lock_count' = [lock_count EXCEPT ![i] = @ + 1]	
	/\ mailbox' = Discard(mailbox, i)
	/\ UNCHANGED << deferred_clock, clock, deferred, clients, members,
	  netmon, monitor >> 

\* ============================================================================
\* ReceiveReply action in ProcessMessages fsm state (2)
\* ============================================================================
PM_ReceiveReply2(i) ==
   /\ Cardinality(await_from[i]) > 1
   /\ mailbox[i] /= << >>
   /\ LET msg == Head(mailbox[i])
     IN /\ msg.mtype = Reply
	/\ await_from' = [await_from EXCEPT ![i] = @ \ {msg.src}]
	/\ mailbox' = Discard(mailbox, i)
	/\ UNCHANGED << lock_count, deferred_clock, deferred,
	  clock, fsm_state, clients, members, netmon, monitor >>

\* ============================================================================
\* We use model values to be able to use symmetry
\* so we use our custom less-than relation
\* ============================================================================
IdLessThan(id1, id2) ==
  Assert(Process = {p1,p2} \/ Process = {p1,p2,p3},
   "Please change IdLessThan according to a new  definition of Process") /\
  IF id1 = p1 THEN TRUE
  ELSE
    IF (id1 = p2 /\ id2 = p3) THEN TRUE
    ELSE FALSE

\* ============================================================================
\* Returns TRUE if (clock1,id1) < (clock2,id2) using
\* logical clocks total order relation
\* id1 never equals id2!
\* ============================================================================
CompareClockOrder(clock1, id1, clock2, id2) ==    
  IF clock1 < clock2 THEN
    TRUE
  ELSE
    IF ((clock1 = clock2) /\ IdLessThan(id1, id2)) THEN
      TRUE
    ELSE
      FALSE

\* ============================================================================
\* ReceiveRequest action in ProcessMessages fsm state
\* Case when {HisClock, Src} < {MyClock, Id}
\* ============================================================================
PM_ReceiveRequest1(i) ==
  /\ mailbox[i] /= << >>
  /\ LET msg == Head(mailbox[i])
    IN /\ msg.mtype = Request
       /\ CompareClockOrder(msg.clock, msg.src, clock[i], i)
       /\ deferred_clock' =
           [deferred_clock EXCEPT ![i] = Max(msg.clock, deferred_clock[i])]
       /\ mailbox' = SendReply(Discard(mailbox, i), i, msg.src)
       /\ UNCHANGED << await_from, clock, deferred, lock_count,
       	 fsm_state, netmon, members, clients, monitor >>

\* ============================================================================
\* ReceiveRequest action in ProcessMessages fsm state
\* ============================================================================
PM_ReceiveRequest2(i) ==
  /\ mailbox[i] /= << >>
  /\ LET msg == Head(mailbox[i])
    IN /\ msg.mtype = Request
       /\ (FALSE = CompareClockOrder(msg.clock, msg.src, clock[i], i))
       /\ deferred_clock' =
           [deferred_clock EXCEPT ![i] = Max(msg.clock, deferred_clock[i])]
       /\ mailbox' = Discard(mailbox, i)
       /\ deferred' = [deferred EXCEPT ![i] = [@ EXCEPT ![msg.src] = TRUE]]
       /\ UNCHANGED << lock_count, await_from, clock, fsm_state,
       	 netmon, clients, members, monitor >>

\* ============================================================================
\* Unlock action in Locked fsm state
\* ============================================================================
Locked_Unlock(i) ==  
  LET deferred_ids == {j \in Process : deferred[i][j] = TRUE}
  IN /\ mailbox' = Broadcast(mailbox, i, deferred_ids, [mtype |-> Reply, src |-> i])
     /\ fsm_state' = [fsm_state EXCEPT ![i] = Initial]
     /\ deferred' = [deferred EXCEPT ![i] = [j \in Process |-> FALSE]]
     /\ UNCHANGED << await_from, lock_count, clock, deferred_clock, members,
       clients, netmon, monitor >>


\* ============================================================================
\* ClientUp message received in ProcessMessages fsm state
\* ============================================================================
PM_ReceiveClientUp(i) ==
  /\ mailbox[i] /= << >>
  /\ LET msg == Head(mailbox[i])
    IN /\ msg.mtype = ClientUp
       /\ IF ( \lnot msg.src \in clients[i] ) THEN
           /\ mailbox' = SendMessage(Discard(mailbox, i), msg.src,
   	     [mtype |-> Request, src |-> i, clock |-> clock[i]])
           /\ clients' = [clients EXCEPT ![i] = @ \cup {msg.src}]
           /\ await_from' = [await_from EXCEPT ![i] = @ \cup {msg.src}]
	   /\ monitor' = [monitor EXCEPT ![i][msg.src] = TRUE]
           /\ UNCHANGED << clock, members, fsm_state, deferred_clock, deferred,
		  lock_count, netmon >>
	 ELSE
	   \* Ignore client up of the client we aware of
	   /\ mailbox' = Discard(mailbox, i)
	   /\ UNCHANGED << clock, members, clients, fsm_state, await_from,
	          deferred_clock, deferred, lock_count, netmon, monitor >>

\* ============================================================================
\* ClientDown message received in ProcessMessages fsm state
\* ============================================================================
PM_ReceiveClientDown(i) ==
  /\ mailbox[i] /= << >>
  /\ LET msg == Head(mailbox[i])
    IN /\ msg.mtype = ClientDown
       /\ await_from' = [await_from EXCEPT ![i] = @ \ {msg.src}]
       /\ clients' = [clients EXCEPT ![i] = @ \ {msg.src}]
       /\ monitor' = [monitor EXCEPT ![i][msg.src] = FALSE] \* no more monitoring
       /\ mailbox' = Discard(mailbox, i)
       /\ IF await_from'[i] = { } THEN
         /\ fsm_state' = [fsm_state EXCEPT ![i] = Locked]
         /\ lock_count' = [lock_count EXCEPT ![i] = @ + 1]
         /\ UNCHANGED << deferred, deferred_clock, clock, netmon, members >>
         ELSE
           UNCHANGED << deferred, deferred_clock, members, clock, fsm_state,
   		    lock_count, netmon >>
				      
OnReceiveClientUp(i) ==
  /\ fsm_state[i] /= ProcessMessages \* This case is in separate action
  /\ fsm_state[i] /= Stopped
  /\ fsm_state[i] /= JoinGroup
  /\ mailbox[i] /= << >>
  /\ LET msg == Head(mailbox[i])
    IN /\ msg.mtype = ClientUp
       /\ clients' = [clients EXCEPT ![i] = @ \cup {msg.src}]
       /\ mailbox' = Discard(mailbox, i)
       /\ monitor' = [monitor EXCEPT ![i][msg.src] = TRUE]
       /\ UNCHANGED << deferred, members, clock, fsm_state,
     	       await_from, deferred_clock, lock_count, netmon >>

OnReceiveClientDown(i) ==
  /\ fsm_state[i] /= ProcessMessages  \* This case is in separate action
  /\ fsm_state[i] /= Stopped
  /\ mailbox[i] /= << >>
  /\ LET msg == Head(mailbox[i])
    IN /\ msg.mtype = ClientDown
       /\ clients' = [clients EXCEPT ![i] = @ \ {msg.src}]
       /\ mailbox' = Discard(mailbox, i)
       /\ monitor' = [monitor EXCEPT ![i][msg.src] = FALSE]
       /\ await_from' = [await_from EXCEPT ![i] = @ \ {msg.src}]
       /\ UNCHANGED << deferred, members, deferred_clock, clock, fsm_state,
	   lock_count, netmon >>
			    
OnWaitStartProtocol(i) ==
  /\ fsm_state[i] = WaitStartProtocol
  /\ \/ WS_JoinGroup(i)    \* A user have asked to start protocol

OnJoinGroup(i) ==
  /\ fsm_state[i] = JoinGroup
  /\ \/ JoinGroup_GetMembers(i)

OnGetMembers(i) ==
  /\ fsm_state[i] = GetMembers
  /\ \/ GetMembers_StartProtocol(i)

OnInitial(i) ==
  /\ fsm_state[i] = Initial
  /\ \/ Initial_Acquire1(i)
    \/ Initial_Acquire2(i)
    \/ Initial_ReceiveRequest(i)

OnProcessMessages(i) ==
  /\ fsm_state[i] = ProcessMessages
  /\ \/ PM_ReceiveReply1(i)
    \/ PM_ReceiveReply2(i)
    \/ PM_ReceiveRequest1(i)
    \/ PM_ReceiveRequest2(i)
    \/ PM_ReceiveClientDown(i)
    \/ PM_ReceiveClientUp(i)
      
OnLocked(i) ==
  /\ fsm_state[i] = Locked
  /\ \/ Locked_Unlock(i)

\* ============================================================================
\* In any moment a process may die; this action represents such behaviour
\* ============================================================================
OnAbnormalTermination(i) ==
  /\ fsm_state[i] /= Stopped
  /\ fsm_state[i] /= WaitStartProtocol
  \* Limit the number of possible crashes
  /\ Cardinality({k \in Process: fsm_state[k] = Stopped}) < MaxCrashes
  /\ fsm_state' = [fsm_state EXCEPT ![i] = Stopped]    
  /\ clients' = [clients EXCEPT ![i] = {}]
  /\ await_from' = [await_from EXCEPT ![i] = {}]
  /\ members' = members \ {i}	
  /\ monitor' = [monitor EXCEPT ![i] = [j \in Process |-> FALSE]]
  /\ UNCHANGED << deferred_clock, deferred, 
        	     clock, lock_count, mailbox, netmon >>

\* If process i has a monitor set for some process j AND
\* process j has died then we must update netmon collection
\* respectively.
NetmonUpdater(i) ==
  LET dead_proc == {j \in Process : fsm_state[j] = Stopped} \* a set of all dead procs
      dead_link == {j \in Process: monitor[i][j] = TRUE /\ j \in dead_proc}
      client_down_msg == {[mtype |-> ClientDown, src |-> j] : j \in dead_link }
      pairs == {i} \times client_down_msg
  IN
     /\ netmon' = netmon \cup pairs
     /\ monitor' = [monitor EXCEPT ![i] =
       [j \in Process |-> IF j \in dead_proc THEN FALSE ELSE monitor[i][j]]]
     /\ UNCHANGED << deferred_clock, deferred, 
        	     clock, lock_count, mailbox, members, clients, fsm_state,
		     await_from >>       
\* ============================================================================
\* An interleaved delivery of ClientDown-messages to processes
\* ============================================================================
NetmonDeliverClientDown ==
  /\ netmon /= { }
  /\ LET next == CHOOSE x \in netmon : TRUE
        dst == next[1]
	msg == next[2]
    IN /\ mailbox' = SendMessage(mailbox, dst, msg)
       /\ netmon' = netmon \ {next}
       /\ UNCHANGED << await_from, deferred_clock, deferred, clients, clock, lock_count,
	      	      fsm_state, members, monitor >>

CoreSteps(i) ==
  \/ OnInitial(i)
  \/ OnProcessMessages(i)
  \/ OnLocked(i)

PlusSteps(i) ==
  \/ OnWaitStartProtocol(i)
  \/ OnJoinGroup(i)
  \/ OnGetMembers(i)
  \/ OnReceiveClientUp(i)
  \/ OnReceiveClientDown(i)
  \/ OnAbnormalTermination(i)
  \/ NetmonUpdater(i)
  \/ NetmonDeliverClientDown

FsmStep(i) == PlusSteps(i) \/ CoreSteps(i)

Next == \E i \in Process : FsmStep(i) 

\* Two processes are not allowed to hold mutex
\* at the same time
SafetyInvariant ==
   \A i,j \in Process : i /= j =>
     (fsm_state[i] = Locked =>
       fsm_state[j] /= Locked)

\* Every issued lock request will be processed or client dies.
LockRequestImpliesLock ==
  \A i \in Process :
    (fsm_state[i] = ProcessMessages) ~>   \* LeadsTo operator
    (fsm_state[i] = Locked \/ fsm_state[i] = Stopped)
    
\* If any of FsmStep action is continously enabled then it will
\* be eventually taken (Weak fairness)
WeakFairness ==
   \A i \in Process : WF_vars(FsmStep(i))

RAPlusSpec == Init /\ [][Next]_vars /\ WeakFairness

--------------------------------------------------------------
THEOREM RAPlusSpec => [] (TypeInvariant
	/\ SafetyInvariant
	/\ LockRequestImpliesLock
)
=============================================================================
