(* ======================================== *)
(* Original Ricart-Agrawala algorithm model *)
(* ======================================== *)
(* Author: Evgeny.Shishkin@infotecs.ru      *)

(* Reference:                               *)
(* "An algorithm for mutual exclusion in computer networks" 
    by G.Ricart and A.Agrawala, 1980        *)

------------------------- MODULE RicartAgrawala --------------------------------
EXTENDS Naturals, Sequences, TLC, FiniteSets

\* A set of process IDs
CONSTANTS p1, p2, p3   \* WARNING: Change IdLessThan accordingly
CONSTANT Process

\* FSM states
CONSTANTS Initial, ProcessMessages, Locked
FsmStates == {Initial, ProcessMessages, Locked}

\* Message types
CONSTANTS Request, Reply

\* Maximum number of times client allowed to ask a lock
\* This is needed to make model state space bounded
CONSTANTS MaxLocksPerClient

RequestMessage == [mtype: { Request }, src: Process, seqnum: Nat]
ReplyMessage == [mtype: { Reply }, src: Process]
Messages == Seq(RequestMessage \cup ReplyMessage)

\* Discrepancies with original 'RA' paper is as follows: 
\*   we call 'our_seq_number' as 'clock' and 'highest_seq_number' as 'deferred_clock'
\*   we call 'reply_count' as 'await_from'
\*   we call 'deferred' as 'deferred'
\*   we do not use 'lock_request', instead fsm_state /= Initial can be used
\*    to represent it

VARIABLE await_from         \* Process -> Nat
VARIABLE deferred           \* Process -> [Process -> BOOLEAN]
VARIABLE clock              \* Process -> Nat
VARIABLE deferred_clock     \* Process -> Nat

\* Those variables are special for our system model: we have mailbox and
\* we represent current program pointer as 'fsm_state'
VARIABLE mailbox      \* Process -> Messages
VARIABLE fsm_state    \* Process -> FsmStates

\* Specially for model checking, not to let state space grow unboundedly
VARIABLE lock_count   \* Process -> Nat

vars ==
  << await_from, deferred, clock,
    deferred_clock, mailbox, fsm_state, lock_count >>

Init ==
  /\ await_from = [i \in Process |-> 0]
  /\ deferred = [i \in Process |-> [j \in Process |-> FALSE]]
  /\ clock = [i \in Process |-> 0]
  /\ deferred_clock = [i \in Process |-> 0]
  /\ mailbox = [i \in Process |-> << >>]
  /\ fsm_state = [i \in Process |-> Initial]
  /\ lock_count = [i \in Process |-> 0]

\*  /\ lock_request = [i \in Process |-> FALSE]

TypeInvariant ==
  /\ await_from \in [Process -> Nat]
  /\ deferred \in [Process -> [Process -> BOOLEAN]]
  /\ clock \in [Process -> Nat]
  /\ deferred_clock \in [Process -> Nat]
  /\ mailbox \in [Process ->  Messages]
  /\ fsm_state \in [Process -> FsmStates]
  /\ lock_count \in [Process -> Nat]

\*  /\ lock_request \in [Process -> BOOLEAN]

Max(A,B) ==
  IF A >= B THEN A ELSE B

\* ============================================================================
\* To allow multiple sends in one step we add Q argument:
\* actual queue on which operation has to be performed.
\* Now we can chain multiple queue operations like this:
\* Discard(SendBye(queue, i, j), i), i, j)
\* ============================================================================
SendMessage(Q, dst, msg) ==     
  [Q EXCEPT ![dst] = Append(@, msg)]

\* Send a message to multiple processes in one step 
Broadcast(MB, src, group, msg) ==
  [i \in Process |-> IF i \in group THEN Append(MB[i], msg) ELSE MB[i]]

Discard(Q, dst) ==
  [Q EXCEPT ![dst] = Tail(@)]
CleanupMsgQueue(Q, dst) ==
  [Q EXCEPT ![dst] = << >>]

SendReply(MB, src, dst) ==
  SendMessage(MB, dst, [mtype |-> Reply, src |-> src])

\* ============================================================================
\* Request message arrived in Initial fsm state, so send Reply message
\* unconditionally "Request_Critical_Section == FALSE"
\* ============================================================================
Initial_ReceiveRequest(i) ==
   /\ mailbox[i] /= << >>
   /\ LET msg == Head(mailbox[i])
     IN /\ msg.mtype = Request
        /\ LET maxseqnum == Max(msg.seqnum, deferred_clock[i])
          IN /\ mailbox' = SendReply(Discard(mailbox, i), i, msg.src)
	     /\ deferred_clock' = [deferred_clock EXCEPT ![i] = maxseqnum]
	     /\ UNCHANGED << deferred, await_from, fsm_state, lock_count,
	          clock >>

\* ============================================================================
\* Acquire action in Initial fsm state
\* ============================================================================
Initial_Acquire(i) ==
   /\ lock_count[i] < MaxLocksPerClient
   /\ LET others == Process \ {i}
         clk == 1 + deferred_clock[i] IN   \* very important!
     /\ mailbox' = Broadcast(mailbox, i, others,
              [mtype |-> Request, src |-> i, seqnum |-> clk])
     /\ clock' = [clock EXCEPT ![i] = clk]
     /\ fsm_state' = [fsm_state EXCEPT ![i] = ProcessMessages]
     /\ await_from' = [await_from EXCEPT ![i] = Cardinality(others) ]
     /\ UNCHANGED << deferred, deferred_clock, lock_count >>
     
\* ============================================================================
\* ReceiveReply action in ProcessMessages fsm state
\* ============================================================================
PM_ReceiveReply1(i) ==
   /\ await_from[i] = 1
   /\ mailbox[i] /= << >>
   /\ LET msg == Head(mailbox[i])
     IN /\ msg.mtype = Reply
	/\ await_from' = [await_from EXCEPT ![i] = 0]
	/\ fsm_state' = [fsm_state EXCEPT ![i] = Locked]
        /\ lock_count' = [lock_count EXCEPT ![i] = @ + 1]	
	/\ mailbox' = Discard(mailbox, i)
	/\ UNCHANGED << deferred_clock, clock, deferred >> 

\* ============================================================================
\* ReceiveReply action in ProcessMessages fsm state (2)
\* ============================================================================
PM_ReceiveReply2(i) ==
   /\ await_from[i] > 1
   /\ mailbox[i] /= << >>
   /\ LET msg == Head(mailbox[i])
     IN /\ msg.mtype = Reply
	/\ await_from' = [await_from EXCEPT ![i] = @ - 1]
	/\ mailbox' = Discard(mailbox, i)
	/\ UNCHANGED << lock_count, deferred_clock, deferred,
	  clock, fsm_state >> 

\* ============================================================================
\* We use model values to be able to use symmetry
\* so we use our custom less-than relation
\* ============================================================================
IdLessThan(id1, id2) ==
  Assert(Process = {p1,p2} \/ Process = {p1,p2,p3},
   "Please change IdLessThan according to a new  definition of Process") /\
  IF id1 = p1 THEN TRUE
  ELSE
    IF (id1 = p2 /\ id2 = p3) THEN TRUE
    ELSE FALSE

\* ============================================================================
\* Returns TRUE if (clock1,id1) < (clock2,id2) using
\* logical clocks total order relation
\* id1 never equals id2!
\* ============================================================================
CompareClockOrder(clock1, id1, clock2, id2) ==    
  IF clock1 < clock2 THEN
    TRUE
  ELSE
    IF ((clock1 = clock2) /\ IdLessThan(id1, id2)) THEN
      TRUE
    ELSE
      FALSE

\* ============================================================================
\* ReceiveRequest action in ProcessMessages fsm state
\* Case when {HisClock, Src} < {MyClock, Id}
\* ============================================================================
PM_ReceiveRequest1(i) ==
  /\ mailbox[i] /= << >>
  /\ LET msg == Head(mailbox[i])
    IN /\ msg.mtype = Request
       /\ CompareClockOrder(msg.seqnum, msg.src, clock[i], i)
       /\ deferred_clock' =
           [deferred_clock EXCEPT ![i] = Max(msg.seqnum, deferred_clock[i])]
       /\ mailbox' = SendReply(Discard(mailbox, i), i, msg.src)
       /\ UNCHANGED << await_from, clock, deferred, lock_count,
       	 fsm_state >>

\* ============================================================================
\* ReceiveRequest action in ProcessMessages fsm state
\* ============================================================================
PM_ReceiveRequest2(i) ==
  /\ mailbox[i] /= << >>
  /\ LET msg == Head(mailbox[i])
    IN /\ msg.mtype = Request
       /\ (FALSE = CompareClockOrder(msg.seqnum, msg.src, clock[i], i))
       /\ deferred_clock' =
           [deferred_clock EXCEPT ![i] = Max(msg.seqnum, deferred_clock[i])]
       /\ mailbox' = Discard(mailbox, i)
       /\ deferred' = [deferred EXCEPT ![i] = [@ EXCEPT ![msg.src] = TRUE]]
       /\ UNCHANGED << lock_count, await_from, clock, fsm_state >>

\* ============================================================================
\* Unlock action in Locked fsm state
\* ============================================================================
Locked_Unlock(i) ==  
  LET deferred_ids == {j \in Process : deferred[i][j] = TRUE}
  IN /\ mailbox' = Broadcast(mailbox, i, deferred_ids, [mtype |-> Reply, src |-> i])
     /\ fsm_state' = [fsm_state EXCEPT ![i] = Initial]
     /\ deferred' = [deferred EXCEPT ![i] = [j \in Process |-> FALSE]]
     /\ UNCHANGED << await_from, lock_count, clock, deferred_clock >>
     
OnInitial(i) ==
  /\ fsm_state[i] = Initial
  /\ \/ Initial_ReceiveRequest(i)
    \/ Initial_Acquire(i)

OnProcessMessages(i) ==
  /\ fsm_state[i] = ProcessMessages
  /\ \/ PM_ReceiveReply1(i)
    \/ PM_ReceiveReply2(i)
    \/ PM_ReceiveRequest1(i)
    \/ PM_ReceiveRequest2(i)
      
OnLocked(i) ==
  /\ fsm_state[i] = Locked
  /\ \/ Locked_Unlock(i)

FsmStep(i) ==
  \/ OnInitial(i)
  \/ OnProcessMessages(i)
  \/ OnLocked(i)

Next == \/ \E i \in Process : FsmStep(i)

\* ============================================================================
\* Two processes are not allowed to hold mutex at the same time
\* ============================================================================
SafetyInvariant ==
   \A i,j \in Process : i /= j =>
     (fsm_state[i] = Locked => fsm_state[j] /= Locked)

\* ============================================================================
\* If any of FsmStep action is continously enabled then it will
\* be eventually taken (Weak fairness)
\* ============================================================================
Fairness ==
   \A i \in Process : WF_vars(FsmStep(i))

RASpec == Init /\ [][Next]_vars /\ Fairness

--------------------------------------------------------------
THEOREM RASpec => [] (TypeInvariant /\ SafetyInvariant)
=============================================================================
