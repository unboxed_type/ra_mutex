Require Import Init Bool NPeano List StepStarTheory Arith Program Fin .
Require Import JRWTactics Clients.
Require Import Bool.
Import ListNotations.

Definition Clock := nat.
Definition RequestSig := (ID*Clock*Clock)%type. (* (requester_id, his_clock, my_clock) *)
Definition ReplySig := (ID*Clock*Clock)%type.   (* (sender_id, his_clock, my_req_clock) *)

Theorem request_sig_dec: 
  forall (i1 i2:RequestSig), {i1 = i2} + {i1 <> i2}.
Proof.
    repeat (try decide equality; try apply id_dec).
Qed.

(* Protocol message types *)
Inductive Msg :=
  | Request: ID -> Clock -> Msg  (* sender id, sender clock *)
(* sender id, sender clock, clock of a request to which that reply corresponds *)
  | Reply: ID -> Clock -> Clock -> Msg. 

Lemma eq_msg_dec:
  forall (m1 m2:Msg), {m1 = m2} + {m1 <> m2}.
Proof.
    repeat (try decide equality; try apply id_dec).
Qed.
Hint Resolve eq_msg_dec.

(* Beware of overloaded 'state' term. Here we use it for describing FSM current status *)
Inductive FsmState :=
  | Initial  
  | ProcessMessages 
  | Locked.    

(* A state of a process *)
Record PState := 
  {
    id: ID;                    (* unique id *)
    clock: Clock;              (* process logical clock *)
    deferred: list RequestSig; (* list of unresponded requests *)
    await_from: list ID;       (* list of ids we are waiting a reply from *)
    mbox: list Msg;            (* message box of a process *)
    fsm_state: FsmState;       (* abstract machine instruction *)
    deferred_clock : Clock;    (* maximum of all received clocks *)

    (* ******************************************************************************* *)
    (* Ghost variables used only for proofs, no control decisions are made using those *)
    (* ******************************************************************************* *)
    received_from: list ReplySig; (* list of arrived reply msgs *)
    reply_sent : list ReplySig    (* list of replies we sent to others *)
  }.
Definition Pinit_state (i: ID) := 
  Build_PState i 0 [] [] [] Initial 0 [] [].

(* Global state *)
Definition GState := ID -> PState.

(* Global init state *)
Definition Ginit_state := fun (i: ID) => Pinit_state i.

Inductive FsmInput :=
  | Lock : ID -> FsmInput
  (* receiver id, sender id, senders clock *)  
  | ReceiveRequest : ID -> ID -> Clock -> FsmInput
  | ReceiveReply   : ID -> ID -> Clock -> FsmInput
  | Unlock : ID -> FsmInput.

Lemma label_dec: forall (l1 l2: FsmInput), {l1 = l2} + {l1 <> l2}.
  Proof.
    repeat (try decide equality; try apply id_dec).
  Qed.

(* Step of the system is measured by triple: 
   old state, step label, new state *)
Definition GStep := GState -> FsmInput -> GState.

Definition update (st:GState) (i: ID) (new:PState) :=
  fun i' => if (id_dec i i') then new else (st i').

(* Put message into the end of process mailbox *)
Definition send (i: ID) (msg: Msg) (st: GState) : GState :=
  update st i
    (Build_PState i (clock (st i)) (deferred (st i))
       (await_from (st i)) (mbox (st i) ++ [msg]) 
          (fsm_state (st i)) (deferred_clock (st i)) 
          (received_from (st i)) (reply_sent (st i))).

(* Remove last message *)
Definition remove_msg (i: ID) (st: GState) : GState :=
  update st i 
    (Build_PState i (clock (st i)) (deferred (st i))
       (await_from (st i)) (tl (mbox (st i)))
          (fsm_state (st i)) (deferred_clock (st i)) 
          (received_from (st i)) (reply_sent (st i)) ).

Definition push_deferred (def: RequestSig) (i: ID) (st: GState): GState :=
  update st i 
    (Build_PState i (clock (st i)) (def::(deferred (st i)))
       (await_from (st i)) (mbox (st i))
         (fsm_state (st i)) (deferred_clock (st i))          
         (received_from (st i)) 
         (reply_sent (st i))).  

Definition update_deferred df i st :=
  update st i 
    (Build_PState i (clock (st i)) df (await_from (st i)) (mbox (st i))
       (fsm_state (st i)) (deferred_clock (st i)) 
       (received_from (st i)) (reply_sent (st i)) ).

Definition update_clock clock i st :=
  update st i
    (Build_PState i clock (deferred (st i))
      (await_from (st i)) (mbox (st i))
         (fsm_state (st i)) (deferred_clock (st i)) 
         (received_from (st i)) (reply_sent (st i))).  

Definition update_await_from af i st :=
  update st i
    (Build_PState i (clock (st i)) (deferred (st i))
      af (mbox (st i)) (fsm_state (st i)) (deferred_clock (st i)) 
      (received_from (st i)) (reply_sent (st i))      
    ).  

Definition update_fsm_state new (i:ID) (st : GState) :=
  update st i
    (Build_PState i (clock (st i)) (deferred (st i))
      (await_from (st i)) (mbox (st i)) new (deferred_clock (st i)) 
      (received_from (st i)) (reply_sent (st i))).  

Definition update_received_from (l: list ReplySig) (i:ID) (st:GState) :=
  update st i
    (Build_PState i (clock (st i)) (deferred (st i))
      (await_from (st i)) (mbox (st i)) (fsm_state (st i)) (deferred_clock (st i)) 
      l (reply_sent (st i)) ).  

Definition update_deferred_clock (c: Clock) (i:ID) (st:GState) :=
  update st i
    (Build_PState i (clock (st i)) (deferred (st i))
      (await_from (st i)) (mbox (st i)) (fsm_state (st i))  c 
      (received_from (st i)) (reply_sent (st i))).  

Definition update_reply_sent (rep: list ReplySig) (i:ID) (st:GState) :=
  update st i
    (Build_PState i (clock (st i)) (deferred (st i))
      (await_from (st i)) (mbox (st i)) (fsm_state (st i))  
      (deferred_clock (st i))
      (received_from (st i)) rep).  

Definition send_request (to: ID) (from: ID) (clk: Clock) (s: GState) :=
  if (id_dec to from) then s else
    send to (Request from clk) s.

Definition send_reply (dst: ID) (dst_clock:Clock) (src: ID) (src_clock: Clock)
           (s: GState) :=
  if (id_dec dst src) then s else  
    send dst (Reply src src_clock dst_clock) s.

Definition receive_request (myId: ID) (s: GState) : GState := 
  let state := s myId in
  let req := hd_error (mbox state) in
  let myClock := clock state in
  let DClock := deferred_clock (s myId) in
  let ReplySent := reply_sent (s myId) in
  match req with
  | Some (Request hisId hisClock) =>
     let DClock' := (max DClock hisClock) in    
      if (lt_dec hisClock myClock) then
        (* lt_vclock hisId hisClock myId myClock *)
        update_deferred_clock DClock' myId
          (update_reply_sent ((hisId,hisClock,myClock)::ReplySent) myId
            (send_reply hisId hisClock myId myClock (remove_msg myId s)))
      else if (eq_nat_dec hisClock myClock) then
          if (id_lt_dec hisId myId) then
            (* lt_vclock hisId hisClock myId myClock *)
            update_deferred_clock DClock' myId
              (update_reply_sent ((hisId,hisClock,myClock)::ReplySent) myId
                (send_reply hisId hisClock myId myClock (remove_msg myId s)))
          else (* myId < hisId *)
            update_deferred_clock DClock' myId
              (push_deferred (hisId,hisClock,myClock) myId (remove_msg myId s))
      else (* hisClock > myClock *)        
        update_deferred_clock DClock' myId
          (push_deferred (hisId,hisClock,myClock) myId (remove_msg myId s))
    | Error =>
      s
  end.

Definition acquire_lock (myId: ID) (st: GState) :=
  update_fsm_state Locked myId st.

Definition receive_reply (myId: ID) (s: GState) := 
  let state := s myId in
  let req := hd_error (mbox state) in
  let myClock := clock state in
  let await_from := await_from state in
  let recv_from := received_from (s myId) in
  let DClock := deferred_clock (s myId) in
  match req with
    | Some (Reply hisId hisClock myReqClock) =>
    (* We process reply message if and only if request clock is equal to ours *)
      if (eq_nat_dec myReqClock myClock) then 
          let await_from' := remove_first id_dec hisId await_from in
          let DClock1 := max DClock hisClock in
          let st' :=
              update_deferred_clock DClock1 myId 
               (update_received_from ((hisId,hisClock,myReqClock)::recv_from) myId             
                (update_await_from await_from' myId 
                 (remove_msg myId s)))
          in 
          match await_from' with
            | [] => acquire_lock myId st'
            | _ => st'
          end
      else
        remove_msg myId s
    | Error =>
      s
  end.

Definition process_messages (myId: ID) (s: GState) :=
  let msg := hd_error (mbox (s myId)) in
  let af := await_from (s myId) in
  match af with
  | [] => acquire_lock myId s
  | _ =>
    match msg with
    | Some (Request _ _) =>
      receive_request myId s
    | Some (Reply _ _ _ ) =>
      receive_reply myId s
    | _ =>
      s
    end
  end.
    
(* Helper function for send_all_requests *)
(* It is conceptually simpler than foldleft *)
Fixpoint send_next_request (l: list ID) myId myClock st0 :=
  match l with
    | [] => st0
    | a :: t => send_next_request t myId myClock 
                  (send_request a myId myClock st0)
  end.
  
Definition send_all_requests (myId: ID) (st: GState) := 
  let myClock := 1 + deferred_clock (st myId) in  
   update_await_from (known_to myId) myId
    (update_clock myClock myId                     
     (update_fsm_state ProcessMessages myId 
      (send_next_request (known_to myId) myId myClock st))).

(* Helper function for unlock *)
(* Send Reply message to every member of l *)
Fixpoint send_next_reply (l:list (ID*Clock*Clock)) myId myClock st0 :=
  match l with
    | [] => st0
    | a :: t => 
      let '(hisId,hisClock,_) := a in
      if id_dec hisId myId then
        st0
      else
        send_next_reply t myId myClock
          (send_reply hisId hisClock myId myClock st0)
  end.

(* Send reply unconditionally when in Initial state *)
Definition uncond_reply (myId: ID) (hisId: ID) (hisClock: Clock) (st: GState) :=
  let ReplySent := reply_sent (st myId) in
  let DClock := (max (deferred_clock (st myId)) hisClock) in
  let MyClock := clock (st myId) in
  update_deferred_clock DClock myId
    (update_reply_sent ((hisId,hisClock,MyClock)::ReplySent) myId
      (send_reply hisId hisClock myId MyClock (remove_msg myId st))).

Definition unlock (myId: ID) (st: GState) := 
  let myClock := clock (st myId) in
  let def := deferred (st myId) in 
  let st' := send_next_reply def myId myClock st in   
  update_reply_sent (def ++ (reply_sent (st myId))) myId
    (update_received_from [] myId  
     (update_fsm_state Initial myId 
        (update_deferred [] myId st'))).

Inductive step: GState -> FsmInput -> GState -> Prop :=
  | stepInitial :
      forall s i,
        fsm_state (s i) = Initial ->
        step s (Lock i) (send_all_requests i s)
  | stepProcessRequestInitial:
      forall s i j j_clk,
        fsm_state (s i) = Initial ->
        hd_error (mbox (s i)) = Some (Request j j_clk) ->
        step s (ReceiveRequest i j j_clk) (uncond_reply i j j_clk s)             
  | stepProcessRequest :
      forall s i j j_clk,
        fsm_state (s i) = ProcessMessages ->
        hd_error (mbox (s i)) = Some (Request j j_clk) ->
        step s (ReceiveRequest i j j_clk) (process_messages i s)
  | stepProcessReply :
      forall s i j j_clk i_clk,
        fsm_state (s i) = ProcessMessages ->
        hd_error (mbox (s i)) = Some (Reply j j_clk i_clk) ->
        step s (ReceiveReply i j j_clk) (process_messages i s)
  | stepLocked:
      forall s i,
        fsm_state (s i) = Locked ->
        step s (Unlock i) (unlock i s).

Module StepStarArgs <: StepStarParams.
  Definition State := GState.
  Definition Label := FsmInput.
  Definition step := step.
End StepStarArgs.
Module StepStar := StepStar StepStarArgs.
Export StepStar.

Hint Unfold
     receive_request
     receive_reply
     unlock
     send_all_requests
     push_deferred
     send_request
     send_reply
     update_deferred_clock
     update_deferred
     update_received_from
     update_reply_sent
     update_fsm_state
     update_await_from
     update_clock
     send_next_request
     send_next_reply
     send
     remove_msg
     acquire_lock
     update
     process_messages
  : defs.

Hint Unfold
     push_deferred
     send_request
     send_reply
     update_deferred_clock
     update_deferred
     update_received_from
     update_reply_sent
     update_fsm_state
     update_await_from
     update_clock
     send
     remove_msg
     update
  : updaters.

Hint Unfold
     process_messages
     acquire_lock
     unlock
     send_all_requests
     uncond_reply
     receive_request
     receive_reply
     : gendefs.