(* Description: 
    @Fin N is a type consisting of N distinct objects. It could be 
    used as a type for IDs.
   Author:
    Evgeniy Shishkin <evgeniy.shishkin@gmail.com>
*)
   

Require Import List Arith Bool BoolEq Program.
Import ListNotations.

(* Fin N is a tuple of N unique elements *)
Inductive Fin : nat -> Type :=
  | Fin1 : forall {n}, Fin (S n)
  | FinN : forall {n}, Fin n -> Fin (S n).

(* How many inhabitants we have in t *)
Fixpoint fin_length {n: nat} (t: Fin n) : nat :=
  match t with
    | @FinN p pf => S (fin_length pf)
    | @Fin1 p => 1
  end.

(*** Examples
Definition fin3'   := (FinN (FinN (@Fin1 0))).
Definition fin3''  := (FinN (@Fin1 1)).
Definition fin3''' := (@Fin1 2).
Definition fin2'   := (FinN (@Fin1 0)).
Definition fin2''  := (@Fin1 1).
Definition fin1'   := (@Fin1 0).
***)
(* For each Fin n there are n inhabitants. This function
   returns c-th inhabitant of type Fin n (starting from zero!) *)
Fixpoint generate_fin_n' (n:nat) (c: nat) : option(Fin n) :=
  match n,c with
    | 0, _ => None 
    | S n',0 => Some (@Fin1 n')
    | S n',S c' => 
      match generate_fin_n' n' c' with
        | Some pf =>
          Some (@FinN n' pf)
        | None =>
          Some (@Fin1 n')
      end
  end.

Theorem exists_in_generate_fin:
  forall {n} (i: Fin n),
    exists c, (generate_fin_n' n c) = Some i /\ c >= 0 /\ c < n.
Proof.
  induction i.
  intros. simpl. exists 0.
  split.
  reflexivity. split. auto. apply lt_O_Sn.
  simpl.
  inversion IHi.
  exists (S x).
  inversion H. 
  rewrite H0.
  split.
  reflexivity.
  split. 
  eapply le_0_n.
  eapply lt_n_S. 
  inversion H1. assumption.
Qed.

(*** Examples :
Eval compute in (generate_fin_n' 3 0). (* =fin3''' *)
Eval compute in (generate_fin_n' 3 1). (* =fin3'' *)
Eval compute in (generate_fin_n' 3 2). (* =fin3'  *)
***)

Fixpoint unbox_option {A:Type} (l: list (option A)) : list A :=
  match l with
    | [] => []
    | Some a::t => a :: unbox_option t
    | None :: t => unbox_option t
  end.

Definition generate_all_fin_n (n:nat) : list (Fin n) :=
  let seq' := seq 0 n in  (* [0;..;n-1] *)  
  let res' := map (fun c => generate_fin_n' n c) seq' in
  let fil' := filter (fun r => 
                        match r with 
                          | None => false 
                          | Some _ => true 
                        end) res' in
  unbox_option fil'.

Eval compute in (generate_all_fin_n 10).

Lemma in_seq_interval:
  forall len start x,
    len <> 0 
    -> x >= start
    -> x < start + len
    -> In x (seq start len).
Proof.
  induction len.
  intuition.
  intros.
  simpl.
  inversion H0.
  left. reflexivity.
  right.
  rewrite H3.

  inversion H2.
  subst m.
  rewrite H3.

  destruct len eqn:Hl.
  rewrite plus_comm in H1. simpl in H1.
  rewrite H3 in H1. apply lt_irrefl in H1. contradiction.
  apply IHlen.
  unfold not. intros. inversion H4; subst; clear H4.
  auto.
  rewrite plus_comm. simpl. 
  apply le_lt_n_Sm. apply le_plus_r.

  apply IHlen.
  unfold not. intros. subst len. subst. rewrite plus_comm in H1. simpl in H1.
  apply lt_not_le in H1. intuition.
  rewrite <- H3.
  apply le_S.
  apply gt_le_S.
  rewrite <- H5.
  apply le_gt_S. assumption.
  rewrite plus_Sn_m.
  rewrite <- plus_n_Sm in H1.
  assumption.
Qed.

Theorem in_map_generate_fin:
  forall {n} (i: Fin n),
    n <> 0 ->
    let seq' := seq 0 n in  (* [0;..;n-1] *)  
    let res' := map (fun c => generate_fin_n' n c) seq' in
    In (Some i) res'.
Proof.
  intros.
  subst res'.
  apply in_map_iff.  
  assert (H' := exists_in_generate_fin i). 
  inversion H'; clear H'.
  inversion H0; clear H0.
  inversion H2; clear H2.
  exists x.
  split.
  assumption.
  subst seq'.
  generalize dependent x.
  induction n. 
  simpl.
  intuition.
  intros.
  simpl.
  inversion H0.
  left. reflexivity.
  right.
  apply in_seq_interval.
  unfold not. intros. rewrite H5 in H3. inversion H0. subst x. inversion H6.
  subst x. subst. apply lt_S_n in H3. apply le_not_gt in H2.
  apply H2 in H3. contradiction. apply gt_le_S.
  apply lt_0_Sn.
  rewrite H4. simpl. assumption.
Qed.

Lemma in_map_in_filter:
  forall {A:Type} l (i:A),
    In (Some i) l
    -> In (Some i) 
        (filter (fun r => 
           match r with 
           | Some _ => true  
           | None => false 
           end) l).
Proof.
  induction l.
  simpl. intuition.
  simpl.
  intros.
  destruct a.
  inversion H.
  inversion H0.
  simpl. left. reflexivity.
  simpl. right. apply IHl.
  assumption.
  inversion H. inversion H0.
  apply IHl. assumption.
Qed.

Lemma in_filter_in_unbox:
  forall {A:Type} (eq_dec : forall (x1 x2:A), {x1 = x2} + {x1 <> x2})  l (i:A),
   In (Some i) l ->
   In i (unbox_option l).
Proof.
  induction l.
  simpl. intuition.

  intros.
  simpl in H.
  simpl.
  inversion H.
  subst a.
  simpl. left. reflexivity.
  destruct a.
  destruct (eq_dec i a).
  subst a.
  simpl. left. reflexivity.
  simpl. right. auto.
  auto.
Qed.

Fixpoint Fin_beq {m n} (t1: Fin m) (t2: Fin n) :=
  match t1, t2 with
    | @Fin1 m', @Fin1 n' => EqNat.beq_nat m' n'
    | @FinN m' pm, @FinN n' pn => Fin_beq pm pn
    | _, _ => false
  end.

Fixpoint Fin_eq {m n} (t1: Fin m) (t2: Fin n) :=
  match t1, t2 with
    | @Fin1 m', @Fin1 n' => EqNat.eq_nat m' n'
    | @FinN m' pm, @FinN n' pn => Fin_eq pm pn
    | _, _ => False
  end.

(*** Examples
Eval compute in (Fin_beq fin3' fin3'). (* true *)
Eval compute in (Fin_beq fin3' fin3''). (* false *)
Eval compute in (Fin_beq fin2' fin2''). (* false *)
***)
Lemma Fin_beq_refl: 
  forall n (t: Fin n), true = Fin_beq t t.
Proof.
  induction t.
  intros.
  simpl. rewrite <- beq_nat_refl. reflexivity.
  simpl. assumption.
Qed.

Lemma Fin_beq_eq:
  forall n (t1: Fin n) (t2: Fin n), 
    true = Fin_beq t1 t2 -> t1 = t2. 
Proof.
  induction t1.
  intros.  
  dependent destruction t2. reflexivity.
  simpl in H. inversion H.
  intros. dependent destruction t2. simpl in H. inversion H.
  simpl in H. 
  f_equal. apply IHt1. assumption.
Qed.

Theorem fin_dec:  
  forall {n:nat} (i i': Fin n), {i = i'} + {i <> i'}.
Proof.
  intros.
  apply BoolEq.eq_dec with (beq := Fin_beq).
  apply Fin_beq_refl.
  apply Fin_beq_eq.
Defined. (* This fact must be marked as Defined, not Qed, to let
            this theorem be unfolded and calculated thus simplified 
            in Eval compute and simpl *)

Hint Resolve fin_dec.

Theorem in_generate_all_fin:
  forall {n} (i: Fin n),
   n <> 0 -> In i (generate_all_fin_n n).
Proof.
  intros.
  unfold generate_all_fin_n.
  apply in_filter_in_unbox.
  auto.
  apply in_map_in_filter.
  apply in_map_generate_fin.
  assumption.
Qed.

(* This function finds p in list l and returns p-s position,
   starting from 0; we use this function to assign every p some natural number *) 
Fixpoint find_fin_pos_aux {n:nat} (p: Fin n) (c:nat) (l: list (Fin n)) : nat :=  
  match l with
    | p' :: t => if (fin_dec p p') then c 
                 else find_fin_pos_aux p (S c) t      
    | [] => 0
  end.

Definition fin_nat {n:nat} (p: Fin n) : nat :=
  find_fin_pos_aux p 0 (generate_all_fin_n n).
