(** All Clients related stuff place in this file *)

Require Import Init Bool NPeano List StepStarTheory Arith Program Fin .
Require Import JRWTactics.
Import ListNotations.

Parameter N: nat.
Definition Clients := Fin.generate_all_fin_n N.
Definition ID := Fin N : Set.
Definition id_dec := @Fin.fin_dec N.
Hint Resolve id_dec.

(* Returns a list of known clients for i-th client *)
Definition known_to (i:ID) := remove (id_dec) i Clients.

Definition id_lt_dec (n m: ID) := lt_dec (fin_nat n) (fin_nat m).
Definition id_lt (n m: ID) := lt (fin_nat n) (fin_nat m).
Hint Resolve id_lt_dec.

Lemma id_neq_sym: forall (i j:ID), i <> j <-> j <> i.
Proof. split; intuition. Qed.
Hint Resolve id_neq_sym.

Lemma id_lt_asym: forall i j, id_lt i j -> ~ id_lt j i.
Proof. unfold not, id_lt. intros i j H H1. apply lt_asym in H. intuition. Qed.

Lemma not_in_id_known_to: forall i, ~ In i (known_to i).
Proof. unfold known_to.  apply remove_In. Qed.

Lemma ite_id_dec_true: 
  forall {A: Type} n (s1 s2: A), (if id_dec n n then s1 else s2) = s1.
Proof. 
  intros. destruct (id_dec n n). reflexivity. intuition. 
Qed.

Lemma ite_id_dec_false: 
  forall {A: Type} n n' (s1 s2: A), 
    n <> n' 
    -> (if id_dec n n' then s1 else s2) = s2.
Proof. 
  intros. destruct (id_dec n n'). intuition. reflexivity. 
Qed.


Fixpoint remove_first {A:Type} (eq_dec: forall (x y:A), {x = y} + {x <> y}) n l :=
  match l with
    | [] => []
    | a :: t => if eq_dec a n then t else a::(remove_first eq_dec n t)
  end.

Lemma in_Clients:
  forall x, Clients <> [] -> In x Clients.
Proof.
  unfold Clients.
  intros.
  apply Fin.in_generate_all_fin.
  unfold not. intros. find_rewrite. unfold generate_all_fin_n in *.
  simpl in *. intuition.
Qed.

(* Deleting non-relevant element of a list will not 
   change elements membership status *)
Lemma in_remove_first_other_in:
  forall l i j l1, 
    i <> j 
    -> In i l 
    -> remove_first id_dec j l = l1 
    -> In i l1.
Proof.
  induction l.
  simpl. intuition.
  intros. simpl in H1.
  destruct (id_dec a j).
  subst a. simpl in H0. inversion H0. intuition.
  subst l. assumption.  simpl in H0.
  inversion H0. subst a. rewrite <- H1.
  simpl. left. reflexivity. subst l1.
  simpl. right. eapply IHl. eassumption. assumption.
  reflexivity.
Qed.

Lemma in_remove_other_in:
  forall l i j l1, 
    i <> j 
    -> In i l 
    -> remove id_dec j l = l1 
    -> In i l1.
Proof.
  induction l.
  simpl. intuition.
  intros. simpl in H1.
  destruct (id_dec a j).
  subst a. 
  destruct (id_dec j j). 
  simpl in H0. inversion H0. symmetry in H2. apply H in H2. contradiction.
  eapply IHl. eassumption. assumption. assumption.
  subst l1.
  simpl. right. eapply IHl. eassumption. inversion H0.
  symmetry in H1. apply H in H1. contradiction. assumption.
  reflexivity.
  subst l1.
  rewrite ite_id_dec_false. simpl in H0.
  inversion H0. subst a.
  simpl. left. reflexivity.
  simpl. right. eapply IHl. eassumption. assumption.
  reflexivity. auto.
Qed.


Lemma in_id_known_to: 
  forall i j, Clients <> [] -> i <> j -> In i (known_to j).
Proof.
  unfold known_to.
  intros.
  eapply in_remove_other_in.
  eassumption.
  eapply in_Clients.
  assumption.
  reflexivity.
Qed.

(* Suppose we have the followimg lemma:
  L1:  i <> j -> f i j = g j
   and we have hypothesis in the context:
  H : P (f i j) 
  We want to replace P (f i j) with P (g j) in H using
  find_neq_erewrite_len L1 
*)
Ltac find_neq_erewrite_lem lem :=
  match goal with
    | [ H1 : ?X <> ?Y, H2 : _ |- _ ] =>
      eapply lem in H1; erewrite H1 in H2
  end.

Ltac find_neq_erewrite_lem' lem :=
  match goal with
    | [ H1 : ?X <> ?Y, H2 : _ |- _ ] =>
      apply id_neq_sym in H1; eapply lem in H1; erewrite H1 in H2
  end.

