(* StepStarTheory: originally written by Mohsen Lesani as a part of 
    his joint work on Chapar certified data store.
*)

Require Import List.
Import ListNotations.

Ltac depremise H :=
  match type of H with
    | forall x : ?T, _ =>
      let x := fresh "x" in
      assert (x: T);
      [ clear H | specialize (H x); clear x ]                  
  end.

Module Type StepStarParams.
  Parameter State: Type.
  Parameter Label: Type.
  Parameter step: State -> Label -> State -> Prop.
End StepStarParams.

Module StepStar (StepStarArgs: StepStarParams).
  Import StepStarArgs.

  Inductive step_star: State -> list Label -> State -> Prop :=
  | refl s: 
      step_star s nil s
  | steps s1 ls s2 l s3: 
      step_star s1 ls s2
      -> step s2 l s3
      -> step_star s1 (ls ++ [l]) s3.

  Definition history (init: State)(ls: list Label): Prop :=
    exists (s: State), step_star init ls s.

  Lemma step_star_end:
    forall s l h s'',
      step_star s (l :: h) s''
      <-> exists s',
           step s l s'
           /\ step_star s' h s''.

    Proof.
      intros.
      split;
      intros.      

      generalize dependent s''.
      induction h using rev_ind.

      intros.
      exists s''.
      split.
      inversion H.
      subst.
      rewrite <- app_nil_l in H0.
      apply app_inj_tail in H0.
      destruct H0; subst.
      inversion H1.
      subst.
      assumption.
      subst.
      apply app_eq_nil in H0.
      destruct H0; subst.     
      inversion H4.
      constructor.

      intros.
      rewrite app_comm_cons in H.
      inversion H.
      rewrite app_comm_cons in H0.
      apply app_inj_tail in H0.
      destruct H0.
      subst.
      apply IHh in H1.
      destruct H1 as [s' [N1 N2]].
      exists s'.
      split.
      assumption.
      econstructor; eassumption.

      (* inverse *)
      generalize dependent s''. 
      induction h using rev_ind.

      intros.
      destruct H as [s' [H1 H2]].
      inversion H2.
      subst.
      clear H2.
      assert (A := steps s nil s l s'').
      depremise A.
      constructor.
      depremise A.
      assumption.
      rewrite app_nil_l in A.
      assumption.

      subst.
      apply app_eq_nil in H.
      destruct H. inversion H3.

      intros.
      destruct H as [s' [H1 H2]].
      remember (h ++ [x]) as hh eqn: Hh.
      destruct H2.
      symmetry in Hh. apply app_eq_nil in Hh.
      destruct Hh.
      subst.
      inversion H0.

      apply app_inj_tail in Hh.
      destruct Hh.
      subst.
      specialize (IHh s2).
      depremise IHh.
      exists s1.
      split.
      assumption.
      assumption.
      rewrite app_comm_cons.
      econstructor; eassumption.      
    Qed.


  Lemma step_star_app:
    forall s h1 h2 s'',
      step_star s (h1 ++ h2) s''
      <-> exists s',
           step_star s h1 s'
           /\ step_star s' h2 s''.

    Proof.
      intros.
      split.

      intros.
      generalize dependent s.
      generalize dependent h2.
      induction h1.
      (* -- *)
      intros.
      rewrite app_nil_l in H.
      exists s.
      split. apply refl. assumption.
      (* -- *)
      intros.
      rewrite <- app_comm_cons in H.
      apply step_star_end in H.
      destruct H as [s' [H1 H2]].
      specialize (IHh1 h2 s').
      depremise IHh1. assumption.
      destruct IHh1 as [sm [IHh1 IHh2]].
      exists sm.
      split. 
        apply step_star_end. exists s'. split; assumption.
        assumption.

      intros.
      generalize dependent s.
      generalize dependent h2.
      induction h1.
      (* -- *)
      intros.
      destruct H as [s' [H1 H2]].
      rewrite app_nil_l.
      inversion H1. 
        subst. assumption.
        subst. destruct ls. rewrite app_nil_l in H. inversion H. inversion H.
      (* -- *)
      intros.
      destruct H as [s' [H1 H2]].
      apply step_star_end in H1.
      destruct H1 as [sm [H11 H12]].
      specialize (IHh1 h2 sm).
      depremise IHh1.
      exists s'. split; assumption.
      rewrite <- app_comm_cons.
      eapply step_star_end.
      exists sm. split; assumption.      

    Qed.


  Lemma step_star_one:
    forall s l s',
      step_star s [l] s'
      <-> step s l s'.

    Proof.
      intros; split; intros.

      inversion H.
      destruct ls.
      subst.
      (* app.*)
      rewrite app_nil_l in H0.
      inversion H0.
      subst.
      inversion H1.
      subst.
      assumption.
      destruct ls.
      rewrite app_nil_l in H2.
      inversion H2.
      inversion H2.
      inversion H0.
      destruct ls.
      rewrite app_nil_l in H7.
      inversion H7.
      inversion H7.

      assert (A : [l] = nil ++ [l]).
      apply app_nil_l.
      rewrite A in *; clear A.
      eapply steps.
      constructor.
      assumption.
    Qed.

  Lemma step_star_app_one:
    forall s h l s'',
      step_star s (h ++ [l]) s''
      <-> exists s',
           step_star s h s'
           /\ step s' l s''.

    Proof.
      intros.
      assert (L := step_star_app).
      specialize (L s h [l] s'').
      split; intros.
      destruct L as [L _].
      depremise L.
      assumption.
      destruct L as [s' [L1 L2]].
      assert (L := step_star_one).
      specialize (L s' l s'').
      destruct L as [L _].
      depremise L.
      assumption.
      exists s'.
      split; assumption.

      destruct L as [_ L].      
      depremise L.
      destruct H as [s' [H1 H2]].
      exists s'.      
      split.
      assumption.
      assert (L := step_star_one).
      specialize (L s' l s'').
      destruct L as [_ L].
      depremise L. assumption.
      assumption.
      assumption.
    Qed.


  Hint Constructors step_star.

End StepStar.