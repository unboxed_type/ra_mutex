# RA+ mutual exclusion protocol

This development represents formalization of Ricart-Agrawala mutual exclusion protocol and
its extension turning it into fault-tolerant and able to carry dynamic process set change
together with proofs and implementation in Erlang.

It uses standard erlang:monitor/2 facility as its failure monitor and pg2 as its group
membership protocol.

## Requirements

 - [`Coq 8.5`](https://coq.inria.fr/download)
 - [`Erlang 18`](https://www.erlang.org/downloads)
 - [`TLA+`](http://lamport.azurewebsites.net/tla/tla.html#tools)

## Artifacts

* src/ra_mutex_ft.erl : Implementation of RA+ algorithm.
		   
* src/ra_mutex_client.erl : Example demonstrating how to use the RA+ mutex
 		    from other Erlang module.
		    
* src/ra_mutex.erl : Original Richard-Argawala algorithm without fault-tolerance,
  	       as could be found in literature.


It also contains the following non-executable proof-oriented artifacts:

* proofs/RaMutex.v     : Formal proof of safety property of original RA mutex
	                 algorithm. Proof is carried in Coq proof assistant.
* proofs/System.v      : Distributed system model formalization
* proofs/Invariants.v  : Facility to support invariant reasoning
* proofs/Fin.v         : Theory for representing client id type, used in RaMutex.v

* proofs/StepStarTheory.v : Step-step relation theory (borrowed from development of Mohsen Lesani)

Models of the system and the algorithm:

* model/RicartAgrawala.tla : TLA+ model of original Ricart-Agrawala algorithm.
* model/RAPlus.tla : TLA+ model of RAPlus algorithm.

## Building implementation

From project root directory execute the following:

```
$ cd src; make
```


## Running/testing ra_mutex on a cluster

For each participating node execute the following:

```
$ erl -sname test1@host -setcookie <Cookie>
```

To make cluster fully connected do the following in erlang shell
for each participating node:

```
(nodename) > net_adm:ping(test1@host).

pong
```

To run test on each maching execute the following:

```
(nodename) > ra_mutex_client:start().
```

Any expected failure scenario must not compromise a safety property of the algorithm.


## Checking proofs

To check Coq proofs one could run the following for project root directory:

```
$ cd proofs; make
```

This will start checking process which could take some time to finish.


## Reference

https://en.wikipedia.org/wiki/Ricart%E2%80%93Agrawala_algorithm


## Questions

All questions and suggestions may be sent using repository issues facility.