%%-------------------------------------------------------------------
%% @author 
%%   Evgeniy Shishkin <evgeniy.shishkin@gmail.com>
%% @doc
%%   Ricart-Agrawala distributed mutual exclusion protocol
%%   without client join/client failure support - so the most
%%   basic version of the algorithm. Its formally verified
%%   model could be found in RaMutex.v; 
%%   It is written as ordinary Erlang module without OTP 
%%   machinary to ensure that no new behaviours is added 
%%   besides those used in formal model.
%% @reference
%%   https://en.wikipedia.org/wiki/Ricart%E2%80%93Agrawala_algorithm
%% @end
%%-------------------------------------------------------------------
-module(ra_mutex).
-compile(export_all).
-type fsm_state() :: 
	initial
      | process_messages 
      | locked.

-record(request, {src, clock}).
-record(reply, {src}).

-record(state, {id :: integer(), 
		clock :: integer(), 
		deferred :: list(#request{}),
		deferred_clock :: integer(),
		%% somehow every process must know about everyone else.
		others :: list(integer()), 
		fsm_state :: fsm_state(),
		await_from :: list(integer()),
		lock_iter :: integer()
}).

-define(ASSERT(X), true = X).
-define(LOG(Text,Args), 
	io:format("{~p,~p,~p}: ~s~n",
		  [node(),?MODULE,?LINE,io_lib:format(Text,Args)])).
-ifdef(McErlang).
-define(put_probe(Id), mce_erl:probe_state({inUse, Id})).
-define(del_probe(Id), 
	mce_erl:choice([fun () -> 1 end, fun () -> 2 end]),
	mce_erl:del_probe_state({inUse, Id})).
-else.
-define(put_probe(Id), undefined).
-define(del_probe(Id), undefined).
-endif.

init_state(Id, Others, LockIter) ->
    #state{id = Id, clock = 0, deferred = [], 
	   deferred_clock = 0, others = Others,
	   fsm_state = initial,
	   await_from = [], lock_iter = LockIter}.
name_from_id(ProcId) ->
    list_to_atom("client" ++ integer_to_list(ProcId)).
pid_from_id(ProcId) ->
    whereis(name_from_id(ProcId)).
send_message(ProcId, Msg) ->
    Pid = pid_from_id(ProcId),
    erlang:send(Pid, Msg).

%% ===========================================================================
%% Broadcast a request for a lock; supply each request with our CLOCK
%% =========================================================================== 
send_all_requests(State) ->
    Clock = 1 + State#state.deferred_clock,
    KnownClients = State#state.others, % nodes() ?
    [send_message(I, #request{src = State#state.id, 
			      clock=Clock}) || I <- KnownClients],
    State#state{fsm_state=process_messages, await_from=KnownClients, clock=Clock}.

%% ===========================================================================
%% Unconditionally reply to a request if we are not trying to acquire 
%% a lock ourself
%% ===========================================================================
uncond_reply(State) ->
    receive
	#request{src=HisId, clock=HisClock} ->
	    MaxDClock = max(HisClock, State#state.deferred_clock),
	    send_message(HisId, #reply{src=State#state.id}),
	    uncond_reply(State#state{deferred_clock=MaxDClock})
	after 0 ->
	    State
    end.

%% ===========================================================================
%%  1) process replies from other processes - from those who gave their permission 
%%     to acquire the lock for us
%%  2) process requests from others - we are going to give our permission only 
%%     if {HisId,HisClock} < {MyId,MyClock}
%%             otherwise put the request into defered list. 
%% If and only if we received all replies we need only then we execute critical
%% section block.
%% ===========================================================================
critical_section(State) ->
    % Do some thing with exclusive resource here
    ?LOG("Client ~p acquired the lock.~n", [State#state.id]),
    LockIter = State#state.lock_iter,
    State#state{fsm_state=locked,lock_iter=LockIter-1}.

process_messages(State)
  when State#state.await_from =:= [] ->
    ?put_probe(State#state.id),
    critical_section(State);

process_messages(State) ->
    MyClock = State#state.clock,
    DClock = State#state.deferred_clock,
    MyId = State#state.id,
    receive
	#reply{src=HisId} ->
            AwaitFrom1 = lists:delete(HisId,State#state.await_from),	  
	    State#state{await_from=AwaitFrom1};
	#request{src=HisId, clock=HisClock}=Req ->
	    if HisClock < MyClock ->
		    send_message(HisId, #reply{src=MyId}),
		    State;
	       true ->	
		    if (HisClock == MyClock) andalso (HisId < MyId) ->
			    send_message(HisId, #reply{src=MyId}),
			    State;
		       true ->
                          % at this point the following condition holds:
			  % HisClock > MyClock Or (HisClock = MyClock andalso HisId > MyId)
                          % in both cases we have to defer our reply for this client.
			    ?ASSERT (HisClock > MyClock orelse 
		  	      (HisClock == MyClock andalso HisId > MyId)),
			    Deferred = State#state.deferred,
			    DClock1 = max(DClock, HisClock),
			    State#state{deferred_clock=DClock1, 
					deferred=[Req|Deferred]}
		    end
	    end
    end.

%% ===========================================================================
%% Unlock phase - send our reply to all deferred requests, so they
%% are able to acquire the lock after us.
%% ===========================================================================
unlock(State) ->
    ?LOG("Client ~p released the lock.~n", [State#state.id]),
    MyId = State#state.id,
    ?del_probe(MyId),
    [begin
	 #request{src=HisId} = Req,
	 send_message(HisId, #reply{src=MyId}) 
     end || Req <- State#state.deferred],
    State#state{deferred   = [],
		fsm_state  =  initial}.


%% ===========================================================================
%% Process enters into and executes this function.
%% ===========================================================================
main(State) ->
    case State#state.fsm_state of
	initial -> 
	    CanLock = State#state.lock_iter > 0,
	    case CanLock of
		true ->
		    main(mce_erl:choice(
			   [{fun send_all_requests/1, [State]}, 
			    {fun uncond_reply/1,[State]}]));
		false ->
		    main(uncond_reply(State))
	    end;
	process_messages ->
	    main(process_messages(State));
	locked ->
	    main(unlock(State))
    end.

%% ===========================================================================
%% This is kind of modelling code. It is not a part of formal model.
%% ===========================================================================
spawnsync_init(State) ->
    Parent = self(),
    ChildPid = erlang:spawn(
      fun() ->
        register(name_from_id(State#state.id), self()),
        erlang:send(Parent, {ready, self()}),
	      receive 
		  go -> 
		      ?LOG("Client ~p started. ~n", [State#state.id]),
		      main(State)
	      end
      end),
    receive 
	{ready, ChildPid} -> ok;
	Msg -> error({unexpected_msg,Msg})
    end,
    ChildPid.

spawnsync_run(Pid) ->
    erlang:send(Pid, go).

model(Clients,LockIter) ->
    AllClients = lists:seq(1, Clients),
    Pids = [begin 
		Others = lists:delete(I, AllClients),
		Init = init_state(I, Others, LockIter),
		spawnsync_init(Init)
	    end || I <- AllClients], 
    % after that line we have all KnownClients
    % spawned and there names are registered, so they are ready
    % to run protocol
    [spawnsync_run(P) || P <- Pids].
       
%% Model checker should run 'model/2' function with 
%% Clients equal to size of process set, LockIter equal to
%% number of times lock is acquired by each process.
