%% ==================================================================
%% @author 
%%   Evgeniy Shishkin <evgeniy.shishkin@gmail.com>
%% @doc
%%   This file contains emulation code for ra_mutex_ft. 
%%   It spawns several clients, all of them try to acquire a lock, 
%%   do some action and then do unlock. 
%% @end
%% ===============================================================


-module(ra_mutex_client).
-compile(export_all).

-define(CLIENTS, 3).  %% Number of simultaneous clients in case of manual testing
-define(ID, node()).
-define(DELAY, 7000). %% how much time the client is going to hold the lock
                      %% before unlocking. This is needed when doing manual testing.
-define(LOG(Text,Args), 
	io:format("{~p,~p,~p}: ~s~n",
		  [time(), ?MODULE,?LINE,io_lib:format(Text,Args)])).
-ifdef(McErlang).
-define(put_probe(Id), 
	?LOG("putting probe ~p", [{locked, Id}]),
	mce_erl:probe_state({locked, Id})).
-define(del_probe(Id), 
	?LOG("deleting probe ~p", [{locked, Id}]),
	mce_erl:del_probe_state({locked, Id})).
-define(interleaving(), 
	mce_erl:choice([fun () -> 1 end, fun () -> 2 end])).
-else.
-define(put_probe(Id), undefined).
-define(del_probe(Id), undefined).
-define(interleaving(), undefined).
-endif.


node_name(Id) ->
    list_to_atom(lists:flatten(io_lib:format("node~p@oberon", [Id]))).

%% In case of manual testing
start() ->
    client(3).

%% In case of running under McErlang
start_mc(NumOfClients, Iters) ->
    [begin 
	 SpawnNode = node_name(I),
	 spawn(SpawnNode, ?MODULE, client, [Iters])
     end 
     || I <- lists:seq(1, NumOfClients)].
    

client(N) ->
    ?LOG("Starting fsm ~p", [?ID]),
    % Somewhy McErlang returns all nodes in nodes() while it should return
    % only 'other' nodes.
    AllNodes = lists:delete(node(), lists:delete('node0@oberon', nodes())),
    ?LOG("AllNodes = ~p, node() = ~p", [AllNodes, node()]),
    AllNodesLen = length(AllNodes),
    FsmClients  = lists:zip(lists:duplicate(AllNodesLen, ra_mutex_ft), AllNodes),
    ?LOG("FsmClients = ~p", [FsmClients]),
    {ok, _} = ra_mutex_ft:start(),
    client_loop(N),
    ra_mutex_ft:stop().

client_loop(0) ->    
    ?LOG("Client ~p quit",[?ID]);    
client_loop(N) ->
    ra_mutex_ft:lock(),    
    %timer:sleep(?DELAY),
    ?put_probe(node()),
    ?interleaving(),
    ?del_probe(node()),
    ra_mutex_ft:unlock(),
    client_loop(N-1).
